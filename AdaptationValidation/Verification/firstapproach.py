'''
   First Compliance checking approach 
'''
import yaml
from decimal import Decimal, getcontext
from sympy import Symbol, Eq, solve

getcontext().prec = 6
precision = 3

def GetReconfStratEvents(transitions):
    events = {}
    for transition in transitions:
        for event in transition['events']:
            if ('type' in event) and (event['type'] == 'Q-Event'):
                events[transition['id']] = []
                events[transition['id']].append(
                    [event['id'],
                    event['predicate']['metric'],
                    event['predicate']['operator'],
                    event['predicate']['refValue']])
    return events

def ThresholdComputation(SLAG, cN):
    def _AvailAggregDecomp(GlobalAvailabilityRate, ServicesNumber):
        X = Symbol('x')
        result = solve(Eq(ServicesNumber *X - ((ServicesNumber*100)-100), GlobalAvailabilityRate))
        return round(float(result[0]),precision)    
    T = {}
    CorrTable = {
        "Availability" : "AvailabilitySumType",
        "ResponseTime" : "MaxType",
        "Cost": "sumType"
    }
    for O in SLAG:
        computeMethod = CorrTable[O['name']]
        if computeMethod == "MaxType":
            w = O['value']
        elif computeMethod == "sumType":
            w = round(float(O['value']) / cN,precision)
        elif computeMethod == "AvailabilitySumType":
            w = _AvailAggregDecomp(float(O['value']),cN)
        T[O['name']] = [w,O['conditionStatement']]
    return T

def ComplianceChecking(R, T):
    #Thresholds metric
    result = {}
    for subSLA in R:
        result[subSLA] = []
        C = R[subSLA][0][1]
        check = 'NOK'
        if C in T:
            print(subSLA, end=": ")
            t = float(T[C][0])
            op = T[C][1]
            ev = R[subSLA][0]
            eID = R[subSLA][0][0]
            eValue = float(R[subSLA][0][3])
            if op == '>=':
                if eValue >= t: 
                    check = 'OK'
            elif op == '>':
                if eValue > t: 
                    check = 'OK'
            elif op == '<=':
                if eValue <= t: 
                    check = 'OK'
            elif op == '<':
                if eValue < t: 
                    check = 'OK'
            ev.insert(4,check)
            #print(ev)
        result[subSLA].append(ev)
    return result

def first_approach(sla):
    # get objectives
    G_SLAobjectives = sla['Objectives']
    # Number of modules
    if 'sub-SLAs' in sla:
        ComponentNumber = len(sla['sub-SLAs'])
    else:
        ComponentNumber = 1
    # Computation of event thresholds
    Thresholds = ThresholdComputation(G_SLAobjectives, ComponentNumber)
    # Get State Machine
    TransitionEvents = GetReconfStratEvents(sla['StateMachine']['Transitions'])
    # Compliance checking 
    result = ComplianceChecking(TransitionEvents,Thresholds)
    for sla in result:
        for event in result[sla]:
            if event[4] == 'NOK':
                return 'ERROR in ', event
    return True