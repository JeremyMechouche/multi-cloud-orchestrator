import os

import pm4py
from pm4py.algo.conformance.alignments.petri_net import algorithm as alignments
from pm4py.objects.petri_net.data_petri_nets import semantics
from pm4py.objects.petri_net.data_petri_nets.data_marking import DataMarking
import pm4pycvxopt

def get_trans_by_name(net, name):
    ret = [x for x in net.transitions if x.name == name]
    if len(ret) == 0:
        return None
    return ret[0]

def execute_script():
    log = pm4py.read_xes(os.path.join("..", "ProcessMining", "Exemple1SLA.xes"))
    net, im, fm = pm4py.read_pnml(os.path.join("..", "ProcessMining", "Exemple1SLA.pnml"))
    #pm4py.view_petri_net(net, im, fm, format="svg")
    aligned_traces = alignments.apply(log, net, im, fm, parameters={"ret_tuple_as_trans_desc": True})
    print(aligned_traces)
    for index, trace in enumerate(log):
        #For each log case 
        aligned_trace = aligned_traces[index]
        # Get alignment based on net 
        al = [(x[0][0], get_trans_by_name(net, x[0][1])) for x in aligned_trace["alignment"]]
        m = DataMarking(im)
        print(m)
        idx = 0
        for el in al:
            if el[1] is not None:
                en_t = semantics.enabled_transitions(net, m, trace[min(idx, len(trace) - 1)])
                if el[1] in en_t:
                    if "guard" in el[1].properties:
                        print(el[1], "GUARD SATISFIED", el[1].properties["guard"], m)
                    m = semantics.execute(el[1], net, m, trace[min(idx, len(trace) - 1)])
                else:
                    print("TRANSITION UNAVAILABLE! Guards are blocking")
            if el[0] != ">>":
                idx = idx + 1


if __name__ == "__main__":
    execute_script()

