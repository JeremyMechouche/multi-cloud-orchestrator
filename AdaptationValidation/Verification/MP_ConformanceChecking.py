import time
import networkx as nx
"""
    Implementation of the algorithm proposed in "Balanced multi-perspective checking of process conformance"
    by F. Mannhardt, M. de Leoni, H. A. Reijers, W. M. P. van der Aalst
    
    Parameters
    -----------------------------------------
    Input
        DPN-net Petri net with data
        Initial and Final Marking
        Log Trace
        Cost Function
    Return
        Balanced Alignment
"""
def ctrl_succ(DPN, Log, Mi, Mf, Node, Graph):
    """
        Return every possible alignment that can be obtained by adding one legal move
        ignoring variables, guards and write operations to a node/alignment 
    """
    print(Node)
    Possible_move = []
    if Node == []:
        Node = Mi
    else:
        Node = Node[-1][0]
    print(DPN.arcs)
    for arc in DPN.arcs:
        if arc.source == Node:
            print(arc)
            #Possible_move.append([arc.target,'>>'])
            Possible_move.append([arc.target, arc.target])
            #Possible_move.append(['>>', arc.target])
    return Possible_move

def augmentWD(DPN, Log, Yc):
    """
        Augmentation of control-flow successor of an alignment/node
    """
    # Ajoute les dimensions données à l'alignement
    # print(Yc)
    for t in DPN.transitions:
        if t == Yc[0] or t == Yc[1]:
            pass
            #print(t.properties)
    return Yc

def pullLowestCost(Q):
    """
        Return the node with the lowest cost from the queue
    """
    minIndicator = 0
    minValue = Q[0][2]
    for v in range(len(Q)):
        if minValue > int(Q[v][2]):
            minIndicator = v
            minValue = int(Q[v][2])
    return minIndicator

def balancedConformance(DPN, Mi, Mf, Log):
    def Cost_Function_K(y):
        if y[0] != y[1]:
            return 2
        else:
            return 0
    def h(y):
        return 1
    def isNotComplete(Y, Mf):
        if Y == []:
            return True
        else:
            if Y[-1][0] == Mf or Y[-1][1] == Mf:
                return False
            else:
                return True
    print("Algorithm Begin ! ")
    print("Looking for alignment between: ", Mi, " and ", Mf)
    # small negligible cost to guarantee termination
    e = 0.1
    # Liste des alignements
    Y = []
    Y_G = nx.Graph()
    # Queue de noeuds à explorer
    Q = []
    # Boucle Tant que l'alignement n'est pas complet à partir des logs
    while isNotComplete(Y, Mf):
        # Pour tout les successeurs (neighbors) possible du noeud actuel
        for Yc in ctrl_succ(DPN, Log, Mi, Mf, Y, Y_G):
            # Try an augmentation of Yc
            y = augmentWD(DPN, Log, Yc)
            if y != 'T':
                # Calcule du coût de l'alignement 
                # K fonction de coût, e*nbre de Mouvement, Fonction Heuristique
                f = Cost_Function_K(y) + e*len(Y) + h(y)
                Q.append((y[0],y[1],f))
                print(Q)
        # Return le lowest Cost next alignment to explore 
        # And remove it from the Queue
        Y.append(Q.pop(pullLowestCost(Q)))
        print(Y)
        time.sleep(5)
    return 'OK'