import gym
from stable_baselines3 import PPO
import os
from env.Dockenv import Dockenv

models_dir = "models/PPO"

env = Dockenv()
env.reset()

model_path = f"{models_dir}/25000.zip"
model = PPO.load(model_path, env=env)

episodes = 10
for ep in range(episodes):
	obs = env.reset()
	done = False
	while not done:
		action, _states = model.predict(obs)
		obs, rewards, done, info = env.step(action)