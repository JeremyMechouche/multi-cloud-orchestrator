import gym
import numpy as np
from gym import spaces

from elasticsearch import Elasticsearch

# Available Actions
ACTION_NOTHING = 0
ACTION_ADD_RESSOURCE = 1
ACTION_REMOVE_RESSOURCE = 2

def CONNECT_elk(IP_ELASTIC):
	return Elasticsearch(IP_ELASTIC, http_auth=('elastic','elastic'), sheme='http', port=9200)

def GET_sla(es):
	res = es.get(index="orchestrator-related", id='sla')
	return res['_version'], res['_source']

def GET_Observation(es):
	## TODO get from Monitoring Observation on resources
	obs = [0.2,0.6,0.5,0.4]
	return np.array(obs)

def GET_Reward(es):
	## TODO get from Monitoring SLO state
	# SLO state + Cost
	reward = 1.0
	return np.float(reward)

def GET_current_State(es):
	res = es.get(index="orchestrator-related", id='plan')
	return res['_source']['state']

def GET_transitions(es):
	_, sla = GET_sla(es)
	return sla['StateMachine']['Transitions']

def GET_Events(es):
	"""
		Return Events
	"""
	output = {}
	_, sla = GET_sla(es)
	for transition in sla['StateMachine']['Transitions']:
        #id = transition['events'][0]['id']
        ### Warning : Get transition ID and not Event ID on purpose
		id = transition['id']
		type = transition['events'][0]['type']
		sourceState = transition['stateSource']
		predicate = transition['events'][0]['predicate']
		event = [id, type, predicate]
		if id not in output:
			output[id]= [type, sourceState, predicate]
	return output

def GET_Actions(es):
	"""
		Return Actions
	"""
	output = {}
	_, sla = GET_sla(es)
	for transition in sla['StateMachine']['Transitions']:
        #id = transition['events'][0]['id']
        ### Warning : Get transition ID and not Event ID on purpose
		id = transition['id']
		type = transition['actions'][0]['type']
		attributes = transition['actions'][0]['attributes']
		action = [id, type, attributes]
		if id not in output:
			output[id]= [type, attributes]
	return output

def ADD_state_transitions(es, state, transition):
	_, sla = GET_sla(es)
	sla['StateMachine']['States'].append(state)
	sla['StateMachine']['Transitions'].append(transition)
	PUT_sla(es, sla)

def PUT_sla(es, sla):
    res = es.index(index="orchestrator-related", id='sla', document=sla)
    return res['result']

class Dockenv(gym.Env):
	"""Custom Environment that follows gym interface"""
	def __init__(self):
		super(Dockenv, self).__init__()
		# Define action and observation space
		# They must be gym.spaces objects
		# Example when using discrete actions:
		self.action_space = spaces.Discrete(3)
		# observations metrics - all within 0-1 range
        # "avgCPUUtilizationHistory",
        # "p90CPUUtilizationHistory",
        # "avgMemoryUtilizationHistory",
        # "p90MemoryUtilizationHistory"
		self.observation_space = spaces.Box(low=0, 
											high=1.0,
											shape=(4,),
											dtype=np.float32)

	def step(self, action):
		done = False
		if action == 1 :
			# Add a new state with transition from the current increasing the number of resources
			## GET current state
			current_state = GET_current_State(self.es)
			## Copy current state to the new one
			new_state = current_state
			## Change name of the new state
			new_state['id'] = f"{new_state['id']}++"
			## Increase the number of resources composing the state 
			resources = []
			for resource in new_state['resources']:
				for key in resource.keys():
					resources.append(key)
					nb_res = resource[key]["number"] = resource[key]["number"] + 1
			## Creating a transition from the current state to the new
			transitions = GET_transitions(self.es)
			new_transition = {}
			new_transition['id'] = f"T{len(transitions)+1}"
			new_transition['stateSource'] = current_state['id']
			new_transition['stateTarget'] = new_state['id']
			new_transition['events'] = []
			event = {}
			event['id'] = f"E{len(GET_Events(self.es)) + 1}"
			event['type'] = "ResourceRelatedEvent"
			event['predicate'] = {
				"metric" : "cpuusage",
				"operator" : ">=",
				"refValue" : 30,
				"time" : 60,
				"unit" : "s"
			}
			new_transition['events'].append(event)
			new_transition['actions'] = []
			action = {}
			action['id'] = f"A{len(GET_Actions(self.es)) + 1}"
			action['type'] = "Scale-out"
			action['attributes'] = {
				'resource': resources,
				'number': nb_res
			}
			new_transition['actions'].append(action)
			ADD_state_transitions(self.es, new_state, new_transition)

		if action == 2:
			# Add a new state with transition from the current decreasing the numbre of resources
			## GET current state
			current_state = GET_current_State(self.es)
			## Copy current state to the new one
			new_state = current_state
			## Change name of the new state
			new_state['id'] = f"{new_state['id']}--"
			resources = []
			for resource in new_state['resources']:
				for key in resource.keys():
					resources.append(key)
					nb_res = resource[key]["number"] = resource[key]["number"] - 1
			## Creating a transition from the current state to the new
			transitions = GET_transitions(self.es)
			new_transition = {}
			new_transition['id'] = f"T{len(transitions)+1}"
			new_transition['stateSource'] = current_state['id']
			new_transition['stateTarget'] = new_state['id']
			new_transition['events'] = []
			event = {}
			event['id'] = f"E{len(GET_Events(self.es)) + 1}"
			event['type'] = "ResourceRelatedEvent"
			event['predicate'] = {
				"metric" : "cpuusage",
				"operator" : ">=",
				"refValue" : 30,
				"time" : 60,
				"unit" : "s"
			}
			new_transition['events'].append(event)
			new_transition['actions'] = []
			action = {}
			action['id'] = f"A{len(GET_Actions(self.es)) + 1}"
			action['type'] = "Scale-in"
			action['attributes'] = {
				'resource': resources,
				'number': nb_res
			}
			new_transition['actions'].append(action)
			ADD_state_transitions(self.es, new_state, new_transition)

		observation = GET_Observation(self.es)
		reward = GET_Reward(self.es)
		if GET_current_State(self.es) == 'Final':
			done = True
		info = {}
		return observation, reward, done, info

	def reset(self):
        # Start Environment 
		## Check if ochestrator is up and Multi-cloud SLA is deployed 
		IP_ELASTIC = '10.0.2.15'
		self.es = CONNECT_elk(IP_ELASTIC)
		if (GET_sla(self.es) != None):
			## Environement is up
			observation = GET_Observation(self.es)
		return observation  # reward, done, info can't be included
