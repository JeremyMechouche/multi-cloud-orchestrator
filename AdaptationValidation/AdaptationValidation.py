"""
    Adaptation & Verification module 
"""
import yaml, os, logging, pika
import asyncio

# import from local verification folder first_approach based on SLA decomposition
from Verification.firstapproach import first_approach
from Verification.MP_ConformanceChecking import balancedConformance

from elasticsearch import Elasticsearch
from datetime import datetime

from time import sleep

IP_RABBITMQ = '10.0.2.15'
IP_ELASTIC = '10.0.2.15'

class sla: 
    """
        Read Multi-Cloud SLA file
    """
    def __init__(self, file):
        with open(file, 'r') as stream:
            try:
                self.SLA = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
    def validate(self):
        pass

##### ElasticSearch ######
def CONNECT_elk(IP_ELASTIC):
    return Elasticsearch(IP_ELASTIC, http_auth=('elastic','elastic'), sheme='http', port=9200)

def GET_sla(es):
    res = es.get(index="orchestrator-related", id='sla')
    return res['_version'], res['_source']

def PUT_sla(es, sla):
    res = es.index(index="orchestrator-related", id='sla', document=sla)
    return res['result']

def PUT_plan(es, plan):
    res = es.index(index="orchestrator-related", id='plan', document=plan)
    return res['result']

def GET_current_State(es):
    res = es.get(index="orchestrator-related", id='plan')
    return res['_source']['state']['type']

def ADD_state_transitions(es, state, transition):
    _, sla = GET_sla(es)
    sla['StateMachine']['States'].append(state)
    sla['StateMachine']['transitions'].append(transition)
    PUT_sla(es, sla)

##### RabbitMQ #####
def connection_mq(ip):
    """
        Connexion to rabbitmq broker server
    """
    connection = pika.BlockingConnection(pika.ConnectionParameters(ip))
    return connection.channel()

def read_data_mq(channel, queue):
    """
        Get data from rabbitmq
    """
    channel.queue_declare(queue=queue)
    method_frame, header_frame, body = channel.basic_get(queue=queue, auto_ack=True)
    return body

def emit_data_mq(channel, queue, message):
    """
        Send data to rabbitmq 
    """
    channel.queue_declare(queue=queue)
    channel.basic_publish(exchange='', routing_key=queue, body=message)
    print(f""" [x] Sent {message}""")

def read_PE_data(channel):
    """
        Read policy enforcement data which send a notification if a transition is triggered
    """
    data = read_data_mq(channel, 'transitionTriggered')
    return data

#### Notification Collector
# Get data from Policy Enforcement
def notification_Collector(channel):
    ### Collect notification from Policy enforcement module 
    msg = read_data_mq(channel,'transitionTriggered')
    if msg != None:
        print(msg)
    return msg

def plan_Definition(es, msg):
    ### Identify the objective near to be violated
    ### Identify the action to perform
    state = None
    ADD_state(es, state)
    ### Put New Plan
    PUT_plan(es, msg)
    return True

def stateMachine_Validator(sla):
    return first_approach(sla)

#### Plan Definition
# Notify Deployment
if __name__ == "__main__":
    ## Log
    logging.basicConfig(level=logging.INFO)

    ## Connect RabbitMQ
    channel = connection_mq(IP_RABBITMQ)

    ## Put CSM
    es = CONNECT_elk(IP_ELASTIC)

    ## Get Multi-Cloud SLA
    sla = sla(os.path.join("JournalMotivExample_docker.yml")).SLA

    ## Initialization of plan to perform by deploiement
    plan = {}

    ## State Machine Validator
    if stateMachine_Validator(sla):
        # State Machine = OK
        logging.info("State Machine valid !")

        # Write SLA in knowledge
        logging.info("Write SLA on elasticsearch...")
        PUT_sla(es, sla)

        # Plan Definition for first initialization
        print("Write Plan on elasticsearch...")
        plan['Resources'] = sla['ServiceCover']['resources']
        for state in sla['StateMachine']['States']:
            if state['type'] == 'Initial':
                plan['state'] = state
        PUT_plan(es, plan)

        ## Wait for notification from Policy Enforcement
        ### Start autonomic loop until final state is reached
        while GET_current_State(es) != 'Final':
            msg = notification_Collector(channel)
            if msg == None:
                sleep(2)
            else:
                logging.info("Notification from policy enforcement ... ")
                logging.info("Begin the plan definition ... ")
                plan_Definition(es, msg)
                logging.info("Plan definition sent ... ")
    else:
        # State Machine = NOK
        logging.info("State Machine is not valid")