#!/bin/sh
timedatectl set-timezone Europe/Paris

#Startup application minimal startup functionnality
cd /home/jeremy/Documents/GIT/multi-cloud-orchestrator/
docker network create elastic

## purge rabbitmq queue
#docker exec rabbitmq rabbitmqctl purge_queue log 
#docker exec rabbitmq rabbitmqctl purge_queue transitionTriggered

## clean env
echo "RM all existing container"
docker stop es01 rabbitmq deployment adaptationvalidation policyenforcement monitoring  beat01 beat02 kib01
docker rm es01 rabbitmq deployment adaptationvalidation policyenforcement monitoring  beat01 beat02 kib01

## Rabbitmq
echo "Start Rabbitmq"
docker run --name rabbitmq -d -p 5672:5672 rabbitmq:latest

## Elasticsearch
echo "Start Elasticsearch"
docker run --name es01 -d --net elastic -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.15.1

## Kibana
echo "Start Kibana"
docker run --name kib01 -d --net elastic -p 5601:5601 -e "ELASTICSEARCH_HOSTS=http://10.0.2.15:9200" docker.elastic.co/kibana/kibana:7.15.1

## MetricBeat
echo "Start Metricbeat"
docker run --name beat01 -d --user=root \
--mount type=bind,source="/home/jeremy/multi-cloud-orchestrator/metricbeat.docker.yml",target="/usr/share/metricbeat/metricbeat.docker.yml" \
--volume="/sys/fs/cgroup:/hostfs/sys/fs/cgroup:ro" \
--volume="/proc:/hostfs/proc:ro" \
--volume="/:/hostfs:ro" \
--volume="/var/run/docker.sock:/var/run/docker.sock:ro" \
docker.elastic.co/beats/metricbeat:7.15.1

## Filebeat
echo "Start Filebeat"
docker run --name beat02 -d --user=root \
--volume="/home/jeremy/multi-cloud-orchestrator/filebeat.docker.yml:/usr/share/filebeat/filebeat.yml:ro" \
--volume="/home/jeremy/multi-cloud-orchestrator/Deployment/logs:/logs" \
--volume="/var/run/docker.sock:/var/run/docker.sock:ro" \
docker.elastic.co/beats/filebeat:7.15.1 filebeat \
-e -strict.perms=false -E output.elasticsearch.hosts=["192.168.56.124:9200"]

## Build
#echo "Deployment: Build"
#docker build -t deployment ./Deployment
#echo "Adaptation & Validation: Build"
#docker build -t adaptationvalidation ./AdaptationValidation
#echo "Policy Enforcement: Build"
#docker build -t policyenforcement ./PolicyEnforcement
#echo "Monitoring: Build"
#docker build -t monitoring ./Monitoring
#echo "Conformance Checker: Build "
#docker build -t conformcheck ./ConformanceChecker

#sleep 30
## Run
#echo "Deployment: Run"
#docker run -d --name deployment --mount type=bind,source=/home/jeremy/Documents/GIT/orchestrationbase/Deployment/logs,target=/home/deployment/logs deployment:latest 
#echo "Adaptation & Validation: Run"
#docker run -d --name adaptationvalidation adaptationvalidation:latest
#echo "Policy Enforcement: Run"
#docker run -d --name policyenforcement policyenforcement:latest
#echo "Monitoring: Run"
#docker run -d --name monitoring monitoring:latest 
#echo "Conformance Checker: Run"
#docker run -d --name conformcheck conformcheck:latest