import logging
import ecs_logging
import os, json
from provider import Docker

class CSM:
    """
        Cloud State Machine 
    """
    def __init__(self, id, logger):
        self.id = id
        self.transitions = []
        self.states = []
        self.resources = {}
        self.actual_state = None
        self.final_state = False
        self.log = logging.getLogger("StateMachine")
        self.log.setLevel(logging.DEBUG)
        handler = logging.FileHandler(os.path.join(".","deployment.json"))
        handler.setFormatter(ecs_logging.StdlibFormatter())
        self.log.addHandler(handler)

    def _enforce_resource(self, resources):
        """
            Create resources for the first state
        """
        self.log.info("Start Resource:")
        for resource in self.resources:
            self.log.info(resource)
            if self.resources[resource].attributes['provider'] == 'Docker':
                self.log.info(f"""Start: {self.resources[resource].id} {self.resources[resource].attributes['image']} {self.resources[resource].attributes['number']} {self.actual_state}""")
                res = Docker.cont()
                res.create(
                    self.resources[resource].id, 
                    self.resources[resource].attributes['image'], 
                    self.resources[resource].attributes['number'], 
                    self.actual_state,
                    self.resources[resource].attributes['ports'],
                    #self.resources[resource].attributes['command'],
                    #self.resources[resource].attributes['env']
                    )
                self.resources[resource].connect = res 

    def _update_resource(self, requirements, state):
        """
            Update resource when a transition occur
        """
        self.log.info("Update Resources:")
        for resource in requirements:          
            for attributes in resource:
                self.log.info(attributes)
                res = self.resources[attributes].connect
                res.scale(resource[attributes]['number'], state)

    def _delete_resource(self,state):
        """
            Delete resources
        """
        self.log.info("Delete Resources:")
        for resource in self.resources:
            self.log.info(f"""Start: {self.resources[resource].id} {self.resources[resource].attributes['image']} {self.resources[resource].attributes['number']} {self.actual_state}""")
            res = self.resources[resource].connect
            res.delete(state)

    def start(self):
        """
            Start the state-machine
        """
        for state in self.states:
            if state.type == "Initial":
                self.actual_state = state.id
                self.log.info(f"""Begin State Machine at {self.actual_state}""")
                self._enforce_resource(state.requirement)

    def change_state(self, transition_ID):
        transition_to_trigger = None
        self.log.info(f"""Transition triggered = {transition_ID}""")
        self.log.info(f"""Actual State = {self.actual_state}""")
        
        for transition in self.transitions:
            if (transition.id == transition_ID) and (transition.source == self.actual_state):
                transition_to_trigger = transition
                break
        if transition_to_trigger == None:
            self.log.info("Transition doesn't exist or is incorrect for the current state!")
        else:
            self.log.info(f"""Transition ok from {transition_to_trigger.source} to {transition_to_trigger.target}""")
            self.actual_state = transition_to_trigger.target
            for state in self.states:
                if state.id == self.actual_state and state.type == 'Final':
                    self.final_state = True
                    self.log.info(f"""State Machine stopped at state {self.actual_state}""")
                    self._delete_resource(self.actual_state)
                elif state.id == self.actual_state:
                    self._update_resource(state.requirement, self.actual_state)
    
    def toJson(self):
        CSM_json = {}
        CSM_json['id'] = self.id
        CSM_json['actual_state'] = self.actual_state
        #CSM_json['resources'] = self.resources
        CSM_json['transitions'] = []
        for transition in self.transitions:
            CSM_json['transitions'].append(transition.__dict__)
        CSM_json['states'] = []
        for state in self.states:
            CSM_json['states'].append(state.__dict__)
        return CSM_json

class State:
    def __init__(self, id, type, requirements):
        self.id = id
        self.type = type
        self.requirement = requirements

class Transition:
    def __init__(self, id, source, target, event, action):
        self.id = id
        self.source = source
        self.target = target
        self.event = event
        self.action = action

class ResourceRequirement:
    def __init__(self, id, name, attributes):
        self.id = id
        self.name = name
        self.attributes = attributes
        self.connect = None

class Event:
    def __init__(self, id, type, predicate, expresion):
        self.id = id
        self.type = type
        self.predicate = predicate
        self.expression = expresion

class ReconfigurationAction:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.actionAttribute = [None]

class ActionAttribute:
    def __init__(self, name, value):
        self.name = name
        self.value = value

