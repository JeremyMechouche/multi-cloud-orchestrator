""" 
    Deployment Engine
"""
import os, sys
import logging, yaml, json, pika

from time import sleep
from StateMachine import *
from elasticsearch import Elasticsearch

IP_RABBITMQ = '10.0.2.15'
IP_ELASTIC = '192.168.56.124'

class sla:
    """
        Read Multi-Cloud SLA file
    """
    def __init__(self, file):
        with open(file, 'r') as stream:
            try:
                self.SLA = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
    def validate(self):
        pass

###### Function for Elasticsearch ########
def GET_sla(es):
    res = es.get(index="orchestrator-related", id='sla')
    return res['_source']

def GET_plan(es):
    res = es.get(index="orchestrator-related", id='plan')
    return res['_version'], res['_source']

def PUT_csm(es, csm):
    res = es.index(index="orchestrator-related", id='csm', document=csm)
    return res['result']

###### Function for Rabbitmq ########
def connection_mq(ip):
    """
        Connexion to rabbitmq broker server
    """
    connection = pika.BlockingConnection(pika.ConnectionParameters(ip))
    return connection.channel()

def read_data_mq(channel, queue):
    """
        Get data from rabbitmq
    """
    channel.queue_declare(queue=queue)
    method_frame, header_frame, body = channel.basic_get(queue=queue, auto_ack=True)
    return body

def emit_data_mq(channel, queue, message):
    """
        Send data to rabbitmq 
    """
    channel.queue_declare(queue=queue)
    channel.basic_publish(exchange='',
                        routing_key=queue,
                        body=message)
    print(f""" [x] Sent {message}""")

def read_PE_data(channel):
    """
        Read policy enforcement data which send a notification if a transition is triggered
    """
    data = read_data_mq(channel, 'transitionTriggered')
    return data

def emit_rules(channel, events):
    """
        Send rules to Policy Enforcement from Multi-Cloud SLA
    """
    emit_data_mq(channel, 'rules', json.dumps(events))

###### Main ########
def main():
    # Initialize
    ## ElasticSearch connection
    es = Elasticsearch(IP_ELASTIC, http_auth=('elastic','elastic'), sheme='http', port=9200)
    ## log
    logging.basicConfig(level=logging.INFO)
    
    ## SLA
    logging.info("Read SLA:")
    SLA = GET_sla(es)

    ## config file with identification for resources
    # conf = read_conf(file("config/*.conf"))
    # Instantiation de la State Machine
    Current_CSM = CSM(SLA['name'], logging)

    ## Instantiation Resources
    for resource in SLA['ServiceCover']['resources']:
        Current_CSM.resources[resource['id']] = ResourceRequirement(resource['id'], 
                                                                    resource['description'], 
                                                                    resource['attributes'])
    logging.info(Current_CSM.resources)
    ## Instantiation States
    for state in SLA['StateMachine']['States']:
        Current_CSM.states.append(
            State(state['id'],
            state['type'],
            state['resources']
        ))

    ## Instantiation Transitions
    for transition in SLA['StateMachine']['Transitions']:
        Current_CSM.transitions.append(
            Transition(transition['id'], 
            transition['stateSource'], 
            transition['stateTarget'], 
            transition['events'], 
            transition['actions']
        ))

    ## Get plan and its version 
    verPlan, plan = GET_plan(es)

    ## Demarrage de la State Machine
    Current_CSM.start()

    ## Send rules to Policy enforcement
    events = []
    for transition in Current_CSM.transitions:
        events.append(transition.event)

    ## Boucle de changement de transition
    PUT_csm(es, Current_CSM.toJson()) 
    while(Current_CSM.final_state != True):
        newPlan = GET_plan(es)
        if verPlan == newPlan[0] : 
            sleep(2)
        else:
            print(newPlan)
            logging.info("New plan defined! Provisioning in progress...")
            Current_CSM.change_state(newPlan[1]['transition'])
            verPlan = newPlan[0]
    logging.info("Final State reach...")
    logging.info("deprovisioning in progress...")

if __name__ == '__main__':
  try:
    main()
  except KeyboardInterrupt:
    logging.info('Interrupted...')
    try:
      sys.exit(0)
    except SystemExit:
      os._exit(0)

