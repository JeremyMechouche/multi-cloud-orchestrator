from kubernetes import client, config 

class kube:
    def __init__(self):
        config.load_kube_config()

    def create(self):
        pass

    def start(self):
        pass

    def stop(self):
        pass

    def scale(self):
        pass

    def delete(self):
        pass