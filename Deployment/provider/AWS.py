"""
    AWS Compute deployment based on AWS SDK
"""
# Add file ~/.aws/config 
# [default]
# region = eu-west-1
# Add file ~/.aws/credntials
# [default]
# aws_access_key_id = ""
# aws_secret_access_key = ""
import boto3, os

class ec2:
    def __init__(self):
        ## check if config exist
        if os.path.isfile("/root/.aws/credentials") == False:
            print("Create credentials file !")
        ## Create resource
        self.client = boto3.client('ec2')
        self.resource = boto3.resource('ec2')
        self.IMAGE_ID = 'ami-0a8e758f5e873d1c1'
        self.instances = []

    def create(self, name):
        self.name = name
        response = self.client.run_instances(
            ImageId=self.IMAGE_ID,
            InstanceType='t2.micro',
            MinCount=1,
            MaxCount=1,
            TagSpecifications=[
                {
                    'ResourceType': 'instance',
                    'Tags': [
                        {
                            'Key': 'Name',
                            'Value': self.name
                        },
                        {
                            'Key': 'case',
                            'Value': 'case1'
                        },
                        {
                            'Key': 'state',
                            'Value': 'S1'
                        },
                    ]
                }
            ]
        )
        for r in response['Instances']:
            self.instances.append(r['InstanceId'])
        for i in self.instances:
            self.resource.Instance(i).wait_until_running()

    ## Start
    def start(self):
        self.client.start_instances(InstanceIds=self.instances)
        for i in self.instances:
            self.resource.Instance(i).wait_until_running()

    ## Stop
    def stop(self):
        for i in self.instances:
            self.resource.Instance(i).stop()
        for i in self.instances:
            self.resource.Instance(i).wait_until_stopped()

    ## Scale
    def scale(self, type, nb):
        for i in range(0,nb):
            # Horizontal
            if type == 'in':
                self.resource.Instance(self.instances[-1]).terminate()
                self.resource.Instance(self.instances[-1]).wait_until_terminated()
                del self.instances[-1]
            elif type == 'out':
                self.create()
            # Vertical scaling to add

    ## Delete
    def delete(self):
        for i in self.instances:
            self.resource.Instance(i).terminate()
        for i in self.instances:
            self.resource.Instance(i).wait_until_terminated()

