import docker
from time import sleep

class cont:
    def __init__(self):
        self.client = docker.DockerClient(base_url='tcp://192.168.56.124:2375')
        self.client_API = docker.APIClient(base_url='tcp://192.168.56.124:2375')
        self.services = self.client.services

    def create(self, name, image, nb, state, ports):
        ### Convert ports to dict
        port={}
        for p in ports:
            port[p[0]]=p[1]
        self.image = image
        self.service = self.services.create(name=name, 
                                            image=self.image,
                                            endpoint_spec=docker.types.EndpointSpec(
                                                ports=port),
                                            container_labels={'state':state})
        if nb > 1:
            self.service.scale(nb)
            self.service.reload()

    def scale(self, nb, state):
        self.service.reload()
        container_spec = docker.types.ContainerSpec(self.image,labels={'state':state})
        docker.APIClient.update_service(self.client_API, 
                                        self.service.id,
                                        self.client_API.inspect_service(self.service.id)['Version']['Index'], 
                                        mode=docker.types.ServiceMode('replicated', nb),
                                        task_template=docker.types.TaskTemplate(container_spec),
                                        fetch_current_spec=True)
        self.service.reload()

    def delete(self, state):
        self.service.reload()
        self.service.update(container_labels={'state':state})
        self.service.reload()
        self.service.remove()

if __name__ == '__main__':
    """
        Test des fonctions
    """
    res1 = cont()
    res2 = cont()
    print("Create")
    res1.create('test','nginx',1,'S1',[[80,80]])
    res2.create('test2', 'nginx', 2, 'S2', [[8080,80]])
    sleep(5)
    print("Scale 5")
    res1.scale(5,'S2')
    res2.scale(5,'S2')
    sleep(5)
    res1.delete('S3')
    res2.delete('S3')