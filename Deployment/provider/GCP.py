"""
    GCP Compute deployment based on GCP SDK
"""
import googleapiclient.discovery

class gcp:
    def __init__(self):
        self.COMPUTE = googleapiclient.discovery.build('compute', 'v1')
        self.PROJECT = "csmterraformtest"
        self.ZONE = "europe-west1-b"
        self.INSTANCES = []

    def wait_for_operation(self):
        while True:
            result = self.COMPUTE.zoneOperations().get(project=self.PROJECT, zone=self.ZONE, operation=self.operation).execute()
            if result['status'] == 'DONE':
                print("done.")
            if 'error' in result:
                print("Error")
                raise Exception(result['error'])
            return result

    def create(self, name):
        self.NAME = name
        image_response = self.COMPUTE.images().getFromFamily(
            project='debian-cloud', family='debian-9').execute()
        source_disk_image = image_response['selfLink']

        machine_type = "zones/%s/machineTypes/g1-small" % self.ZONE
        config = {
            'name': name,
            'machineType': machine_type,
            'disks': [
            {
                'boot': True,
                'autoDelete': True,
                'initializeParams': {
                    'sourceImage': source_disk_image,
                }
            }
            ],
            'networkInterfaces': [{
                'network': 'global/networks/default',
                'accessConfigs': [
                    {'type': 'ONE_TO_ONE_NAT', 'name': 'External NAT'}
                ]
            }],
            'metadata':{
                'items': [{
                    'key': 'case',
                    'value': 'case1'
                },
                {
                    'key': 'state',
                    'value': 'S1'
                }]
            }
        }
        self.operation = self.COMPUTE.instances().insert(
            project=self.PROJECT, 
            zone=self.ZONE, 
            body=config).execute()

    def start(self):
        self.operation = self.COMPUTE.instances().start(
            project=self.PROJECT,
            zone=self.ZONE,
            instance=self.NAME).execute()

    def stop(self):
        self.operation = self.COMPUTE.instances().stop(
            project=self.PROJECT,
            zone=self.ZONE,
            instance=self.NAME).execute()

    def scale(self):
        pass

    def delete(self):
        self.operation = self.COMPUTE.instances().delete(
            project=self.PROJECT,
            zone=self.ZONE,
            instance=self.NAME).execute()