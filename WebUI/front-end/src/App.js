import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useState } from "react";
import axios from "axios";

function App() {
  function startSupportFunction(){
    fetch('http://127.0.0.1:5000/start_support');
  }

  function startOrchestrator(){
    fetch('http://127.0.0.1:5000/start_orchestrator');
  }

  function stopSupportFunction(){
    fetch('http://127.0.0.1:5000/stop_supportFunction');
  }

  function stopOrchestrator(){
    fetch('http://127.0.0.1:5000/stop_orchestrator');
  }

  const [sla, setSla] = useState(null);
  
  const apiURL = "http://127.0.0.1:5000/sla";

  const fetchData = async () => {
    const response = await axios.get(apiURL);
    setSla(response.data);
  };

  // Adaptation & Validation
  const [logAV, setlogAV] = useState(null);
  const fetchAdaptValidLogs = async () => {
    const response = await axios.get('http://127.0.0.1:5000/getadptValidLogs');
    setlogAV(response.data);
  };
  // Deployment
  const [logDeploy, setlogDeploy] = useState(null);
  const fetchDeployLogs = async () => {
    const response = await axios.get('http://127.0.0.1:5000/getDeployLogs');
    setlogDeploy(response.data);
  };
  // Policy Enforcement
  const [logPE, setlogPE] = useState(null);
  const fetchPELogs = async () => {
    const response = await axios.get('http://127.0.0.1:5000/getpolicyEnforceLogs');
    setlogPE(response.data);
  };
  // Monitoring
  const [logMonitor, setlogMonitor] = useState(null);
  const fetchMonitorLogs = async () => {
    const response = await axios.get('http://127.0.0.1:5000/getMonitorLogs');
    setlogMonitor(response.data);
  };
  function getLogs(){
    fetchAdaptValidLogs(); fetchDeployLogs(); fetchPELogs(); fetchMonitorLogs();
  }

  return (
    <div className="App">
      <header>
      </header>
        <h1>OpenSLA : Multi-Cloud SLA Orchestrator</h1>
        <h2>Multi-Cloud SLA Representation</h2>
        <div class="container-fluid">
        <div class="row">
          <div class="col-sm">
          <button type="button" class="btn btn-primary btn-lg" onClick={fetchData}>
          Fetch SLA
          </button>
          </div>
          </div>
        <div class="row">
          <div class="col-sm">
          <button type="button" class="btn btn-primary btn-lg" onClick={startSupportFunction}>Start support function</button>
          {

          }
          </div>
          <div class="col-sm">
          <button type="button" class="btn btn-primary btn-lg" onClick={startOrchestrator}>Start Orchestrator</button>
          </div>
          <div class="col-sm">
          <button type="button" class="btn btn-primary btn-lg" onClick={stopSupportFunction}>Stop support function</button>
          </div>
          <div class="col-sm">
          <button type="button" class="btn btn-primary btn-lg" onClick={stopOrchestrator}>Stop Orchestrator</button>
          </div>
        </div>
        </div>
        <div className='sla'>
        {sla && 
        <p>
          Name: {sla.name}
          StartDate: {sla.validityPeriod.start}
        </p>
        }
        </div>
        <br></br>
        <h2>Multi-Cloud SLA Enforcement</h2>
        <button type="button" class="btn btn-primary btn-lg" onClick={getLogs}>Get Logs</button>
        <br></br>
        <div class="container-fluid">
        <div class="row">
          <div class="col-sm">
          <div class="terminal" id="AdptValidation">
            <p class="header">Adaptation Validation log</p>
            <ul>
              {logAV && 
                logAV.map((log, index) =>{
                  return(
                    <li>{log}</li>
                  );
              })}
            </ul>
            </div>
          </div>
          <div class="col-sm">
            <div class="terminal" id="Deploy">
            <p class="header">Deployment log</p>
            <ul>
              {logDeploy && 
                logDeploy.map((log, index) =>{
                  return(
                    <li>{log}</li>
                  );
              })}
            </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm">
          <div class="terminal" id="PolicyEnforcement">
          <p class="header">Policy Enforcement log</p>
          <ul>
            {logPE && 
              logPE.map((log, index) =>{
                return(
                  <li>{log}</li>
                );
            })}
          </ul>
          </div>
          </div>
          <div class="col-sm">
            <div class="terminal" id="Monitor">
            <p class="header">Monitor log</p>
            <ul>
              {logMonitor && 
                logMonitor.map((log, index) =>{
                  return(
                    <li>{log}</li>
                  );
              })}
            </ul>
            </div>
          </div>
        </div>
        </div>
    </div>
  );
}

export default App;
