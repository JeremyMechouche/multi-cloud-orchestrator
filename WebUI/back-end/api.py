from flask import Flask, Response, stream_with_context
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS as Cors

from elasticsearch import Elasticsearch
import docker, socket, time, json

from time import sleep

# Required to start flask application
app = Flask(__name__)
Cors(app)
api = Api(app)

# Required if started on the same server
client_docker = docker.from_env()

def check_open_port(port):
    a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)   
    if a_socket.connect_ex(('127.0.0.1',port)) == 0:
        return True
    else:
        return False

class SLA(Resource):
    def get(self):
        IP_ELASTIC = '127.0.0.1'
        es = Elasticsearch( IP_ELASTIC, http_auth=('elastic','elastic'), sheme='http', port=9200)
        res = es.get(index="orchestrator-related", id='sla')
        return res['_source']

class start_support(Resource):
    def get(self):
        #### Start Orchestrator 
        print("Starting support functions")
        try:
            # Support containers
            cRbmq = client_docker.containers.run(image="rabbitmq:latest", name="rabbitmq", ports={5672:5672}, detach=True, remove=True)
            print("1/4")
        except docker.errors.APIError as e :
            print(e)
        
        try:
            cES = client_docker.containers.run(image="docker.elastic.co/elasticsearch/elasticsearch:7.15.1", name="es01", ports={9200:9200, 9300:9300}, environment={'discovery.type':'single-node'} , detach=True, remove=True)
            print("2/4")
        except docker.errors.APIError as e:
            print(e)
        
        try:
            cBeat1 = client_docker.containers.run(image="docker.elastic.co/beats/metricbeat:7.15.1", name="beat01", user="root", volumes={'/home/jeremy/multi-cloud-orchestrator/WebUI/back-end/metricbeat.docker.yml':{'bind':'/usr/share/metricbeat/metricbeat.yml', 'mode':'ro'}, '/sys/fs/cgroup':{'bind':'/hostfs/sys/fs/cgroup', 'mode':'ro'},'/proc':{'bind':'/hostfs/proc', 'mode':'ro'}, '/':{'bind':'/hostfs', 'mode':'ro'},'/var/run/docker.sock':{'bind':'/var/run/docker.sock','mode':'ro'}}, detach=True, remove=True)
            print("3/4")
        except docker.errors.APIError as e:
            print(e)

        try:
            cBeat2 = client_docker.containers.run(image="docker.elastic.co/beats/filebeat:7.15.1", name="beat02", user="root", volumes={'/home/jeremy/multi-cloud-orchestrator/WebUI/back-end/filebeat.docker.yml':{'bind':'/usr/share/filebeat/filebeat.yml', 'mode':'ro'}, '/home/jeremy/multi-cloud-orchestrator/Deployment/logs':{'bind':'/logs', 'mode':'rw'}, '/var/run/docker.sock':{'bind':'/var/run/docker.sock','mode':'ro'}}, command="filebeat", environment={'strict.perms':'false', 'output.elasticsearch.hosts':["192.168.56.124:9200"]}, detach=True, remove=True)
            print("4/4")
        except docker.errors.APIError as e:
            print(e)
        
        return(True)

class start_orchestrator(Resource):
    def get(self):
        try:
            adaptValid = client_docker.containers.run(image="opensla/adaptvalidation", name="adptvalid", detach=True, remove=True)
            print("1/4")
            sleep(5)
        except docker.errors.APIError as e :
            print(e)
        try:
            poliEnforc = client_docker.containers.run(image="opensla/policyenforcement", name="policyenforce", detach=True, remove=True)
            print("2/4")
        except docker.errors.APIError as e :
            print(e)
        try:
            Monitoring = client_docker.containers.run(image="opensla/monitoring", name="monitor", detach=True, remove=True)
            print("3/4")
        except docker.errors.APIError as e :
            print(e)
        try:
            Deploiemen = client_docker.containers.run(image="opensla/deployment", name="deploy", detach=True, remove=True)
            print("4/4")
        except docker.errors.APIError as e :
            print(e)
        return(True)

class stop_Support_Function(Resource):
    def get(self):
        containers = ["rabbitmq", "es01", "beat01", "beat02"]
        # Get container object based on their name then stop it 
        [client_docker.containers.get(c).stop() for c in containers]

class stop_orchestrator(Resource):
    def get(self):
        containers = ["adptvalid", "policyenforce", "monitor", "deploy"]
        # Get container object based on their name then stop it 
        [client_docker.containers.get(c).stop() for c in containers]
        services = ["Auth", "UI", "Stor"]
        [client_docker.services.get(s).remove() for s in services]

class getDeployLogs(Resource):
    def get(self):
        deploy = client_docker.containers.get("deploy")
        logs = deploy.logs(tail=10)
        logs_list = logs.decode('UTF-8').split('\n')
        return logs_list

class getadptValidLogs(Resource):
    def get(self):
        adptValid = client_docker.containers.get("adptvalid")
        logs = adptValid.logs(tail=10)
        logs_list = logs.decode('UTF-8').split('\n')
        return logs_list

class getpolicyEnforceLogs(Resource):
    def get(self):
        policyEnforce = client_docker.containers.get("policyenforce")
        logs = policyEnforce.logs(tail=10)
        logs_list = logs.decode('UTF-8').split('\n')
        return logs_list

class getMonitorLogs(Resource):
    def get(self):
        monitor = client_docker.containers.get("monitor")
        logs = monitor.logs(tail=10)
        logs_list = logs.decode('UTF-8').split('\n')
        return logs_list

#USE STREAM LOG FUNCTION 
#import docker
#client = docker.from_env()
#container_names = ['container1','container2','container3']
#for container_name in container_names:
#    dkg = client.containers.get(container_name).logs(stream = True, follow = False)
#    try:
#      while True:
#        line = next(dkg).decode("utf-8")
#        print(line)
#    except StopIteration:
#      print(f'log stream ended for {container_name}')  

#Flask stream as a response
#https://stackoverflow.com/questions/39272072/flask-send-stream-as-response
#



class getMonitorLogsStream(Resource):
    def get(self, stream=True):
        monitor = client_docker.containers.get("monitor")
        logs = monitor.logs(stream=True)
        return Response(next(logs).decode('UTF-8').iter_content(chunk_size=10*1024))
        

api.add_resource(SLA, '/sla')
api.add_resource(start_support, '/start_support')
api.add_resource(start_orchestrator, '/start_orchestrator')
api.add_resource(stop_Support_Function, '/stop_supportFunction')
api.add_resource(stop_orchestrator, '/stop_orchestrator')

api.add_resource(getadptValidLogs, '/getadptValidLogs')
api.add_resource(getDeployLogs, '/getDeployLogs')
api.add_resource(getpolicyEnforceLogs, '/getpolicyEnforceLogs')
api.add_resource(getMonitorLogs, '/getMonitorLogs')

api.add_resource(getMonitorLogsStream, '/getMonitorLogsStream')

if __name__ == '__main__':
    app.run()

