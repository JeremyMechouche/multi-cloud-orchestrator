"""
    Unified library of elk function
"""

def CONNECT_elk(IP_ELASTIC):
    return Elasticsearch(IP_ELASTIC, http_auth=('elastic','elastic'), sheme='http', port=9200)

def PUT_sla(es, sla):
    res = es.index(index="orchestrator-related", id='sla', document=sla)
    return res['result']

def PUT_plan(es, plan):
    res = es.index(index="orchestrator-related", id='plan', document=plan)
    return res['result']

def PUT_csm(es, csm):
    res = es.index(index="orchestrator-related", id='csm', document=csm)
    return res['result']

def GET_sla(es):
    res = es.get(index="orchestrator-related", id='sla')
    return res['_source']

def GET_plan(es):
    res = es.get(index="orchestrator-related", id='plan')
    return res['_version'], res['_source']

def GET_Objectives(es):
    return GET_sla(es)['Objectives']

def GET_Events(es):
    """ 
        Function returning unique events in transitions
    """
    output = []
    sla = GET_sla(es)
    for transition in sla['StateMachine']['Transitions']:
        id = transition['events'][0]['id']
        type = transition['events'][0]['type']
        predicate = transition['events'][0]['predicate']
        event = [id, type, predicate]
        if event not in output:
            output.append([id, type, predicate])
    return output

def GET_Resources(es):
    """
        Return list of resources
    """
    output = []
    sla = GET_sla(es)
    for resource in sla['ServiceCover']['resources']:
        output.append(resource)
    return output
