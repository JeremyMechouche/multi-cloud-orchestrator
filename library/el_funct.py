from datetime import datetime
from elasticsearch import Elasticsearch
import time
es = Elasticsearch('192.168.56.124',
        http_auth=('elastic','elastic'),
        sheme='http',
        port=9200)

doc = {
    'name': 'CSM1',
    'States': ['S1','S2','S3'],
    'timestamp': datetime.now()
}

def PUT_csm(es, csm):
    res = es.index(index="orchestrator-related", id='csm', body=doc)
    return res['result']

def GET_csm(es):
    res = es.get(index="orchestrator-related", id='csm')
    return res['_version'], res['_source']

print(PUT_csm(es, doc))

ver, csm = GET_csm(es)

while(True):
    while(ver == GET_csm(es)[0]):
        print("RAS")
        time.sleep(2)
    ver = GET_csm(es)[0]
    print("New Version")
