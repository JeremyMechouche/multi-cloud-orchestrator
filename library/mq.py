"""
    Unified library of rabbitmq function
"""

def connection_mq(ip):
    """
        Connexion to rabbitmq broker server
    """
    connection = pika.BlockingConnection(pika.ConnectionParameters(ip))
    return connection.channel()

def read_data_mq(channel, queue):
    """
        Get data from rabbitmq
    """
    channel.queue_declare(queue=queue)
    method_frame, header_frame, body = channel.basic_get(queue=queue, auto_ack=True)
    return body

def emit_data_mq(channel, queue, message):
    """
        Send data to rabbitmq 
    """
    channel.queue_declare(queue=queue)
    channel.basic_publish(exchange='', routing_key=queue, body=message)
    print(f""" [x] Sent {message}""")

def read_PE_data(channel):
    """
        Read policy enforcement data which send a notification if a transition is triggered
    """
    data = read_data_mq(channel, 'transitionTriggered')
    return data