"""
    Policy Enforcement
"""
import yaml, json
import os, sys
import clips
from datetime import datetime
import pika
import logging
import time
from pprint import pprint

from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search

IP_RABBITMQ = "10.0.2.15"
IP_ELASTIC = '10.0.2.15'

# ElasticSearch
def connect_elasticsearch():
    connected = False
    _es = Elasticsearch([{'host': '10.0.2.15', 'port':'9200'}])
    try:
        if _es.ping():
            print('Connected!')
            connected = True
    except Exception as ex:
        print(str(ex))
    finally:
        return connected, _es

def read_data(client, index, body):
    """
        Generic function to read data from elastic search base
    """
    response = client.search(
        index=index, 
        body=body,
        size=1000
    )
    client.indices.refresh(index=index)
    #print(response)
    response = response["hits"]["hits"]
    return response

def GET_current_State(es):
    res = es.get(index="orchestrator-related", id='plan')
    return res['_source']['state']['type']

def GET_sla(es):
    res = es.get(index="orchestrator-related", id='sla')
    return res['_version'], res['_source']

def GET_Events(es):
    """ 
        Function returning unique events in transitions
    """
    output = {}
    _, sla = GET_sla(es)
    for transition in sla['StateMachine']['Transitions']:
        #id = transition['events'][0]['id']
        ### Warning : Get transition ID and not Event ID on purpose
        id = transition['id']
        type = transition['events'][0]['type']
        sourceState = transition['stateSource']
        predicate = transition['events'][0]['predicate']
        event = [id, type, predicate]
        if id not in output:
          output[id]= [type, sourceState, predicate]
    return output

### Rabbit MQ
def connection_mq(ip):
  connection = pika.BlockingConnection(pika.ConnectionParameters(ip))
  return connection.channel()

def read_data_mq(channel, queue):
  """
    Get logs from rabbitmq
  """
  channel.queue_declare(queue=queue)
  method_frame, header_frame, body = channel.basic_get(queue=queue, auto_ack=True)
  return body

def emit_data_mq(channel, queue, message):
  channel.queue_declare(queue=queue)
  channel.basic_publish(exchange='',
                        routing_key=queue,
                        body=message)
  logging.info(f""" [x] Sent {message}""")

def read_logs_data(channel):
  data = read_data_mq(channel,'log')
  return data

def send_triggered_transitions(channel, transition_triggered):
  message = {
    'transition': transition_triggered,
    'triggered': 'yes'
  }
  emit_data_mq(channel, 'transitionTriggered', json.dumps(message))
  print(message)

def convert_date_to_timestamp(date_str):
  date = datetime.strptime(date_str,"%Y-%m-%d %H:%M:%S")
  timestamp = int(datetime.timestamp(date))
  return timestamp

def unify_timestamp(date_str):
  date = datetime.strptime(date_str,"%Y-%m-%dT%H:%M:%S.%fZ")
  timestamp = int(datetime.timestamp(date))
  return timestamp

def Generate_rules_from_events(es, env):
  transitions = GET_Events(es)
  for event in transitions:
    id = event
    type = transitions[event][0]
    sourceState = transitions[event][1]
    predicate = transitions[event][2]
    rule = None
    if type == 'TemporalEvent':
      logging.info("Rule for a temporal event created...")
      rule = f"""(defrule {id}
      (log
        (timestamp ?datetime&:(> ?datetime {convert_date_to_timestamp(predicate['timestamp'])}))
      )
      =>
        (send_triggered_transitions))
      """
      logging.debug(rule)
    elif type == 'ResourceRelatedEvent':
      logging.info("Rule for resource related event created...")
      rule = f"""(defrule {id}
      (log 
      (eventType "Q-Event")
      (state_source "{sourceState}")
      (metric "{predicate['metric']}")
      (value ?value&:({predicate['operator']} ?value {predicate['refValue']}))
      )
      =>
        (send_triggered_transitions))
      """
      logging.debug(rule)
    else:
      logging.info('Unknown type: No rule create!')
    if rule != None: env.build(rule)
    for rule in env.rules():
      print(rule)

def main():
  ## Initialization of rabbitmq connection
  channel = connection_mq(IP_RABBITMQ)

  ## Initialize Elasticsearch connection
  connected, es = connect_elasticsearch()

  ## Initialized Logging
  logging.basicConfig(level=logging.INFO)

  ## Initialization of CLIPS
  logging.info("Initialization CLIPS")
  env = clips.Environment()

  ## Definition of event templates for Temporal Event and Resource Related Events
  logging.info("Definition des templates")
  env.build("""(deftemplate event_TP
    (slot id (type STRING))
    (slot StateSource (type STRING))
    (slot type (type STRING))
    (slot predicate (type INTEGER)))
    """)
  env.build("""(deftemplate event_RR
    (slot id (type STRING))
    (slot stateSource (type STRING))
    (slot type (type STRING))
    (slot metric (type STRING))
    (slot operator (type STRING))
    (slot value 
      (type INTEGER)
      (range 1 100))
    (slot time (type INTEGER))
    (slot unit (type STRING))
    )
    """)
  env.build("""(deftemplate log
    (slot source (type STRING))
    (slot case (type STRING))
    (slot state_source (type STRING))
    (slot resource_provider (type STRING))
    (slot resource_hostname (type STRING))
    (slot resource_zone (type STRING))
    (slot timestamp (type INTEGER))
    (slot eventType (type STRING))
    (slot metric (type STRING))
    (slot value (type FLOAT))
    )""")
  
  ## Definition of notification orchestrator function in CLIPS
  logging.info("Definition des fonctions dans CLIPS")
  env.define_function(send_triggered_transitions)

  # Get rules from Deployment and create associated rules in clips
  logging.info("Get sla from Knowledge...")
  verSLA, sla = GET_sla(es)
  logging.info("Get sla from Knowledge... OK")

  logging.info("Converting events in rules...")
  Generate_rules_from_events(es, env)

  logging.info("Converting events in rules... OK")

  logging.info("Get logs from monitoring...")
  Ttriggered_log = {}

  ## Etat Final de la state machine n'est pas atteint
  while GET_current_State(es) != 'Final':
    # Update State Machine
    newVerSLA, _ = GET_sla(es)
    if newVerSLA != verSLA:
      logging.info("Add new states and transitions...")
      Generate_rules_from_events(es, env)
      verSLA = newVerSLA

    # Get Data from monitoring
    data = read_logs_data(channel)
    if data != None:
      logging.info(data)
      data = json.loads(data)
      logging.info(data['source'])
      # Check Data from monitoring
      tplt_log = env.find_template('log')
      try:
        tplt_log.assert_fact(source=data['source'],
                             case=data['case'],
                             state_source=data['state'],
                             resource_provider=data['resource_provider'],
                             resource_zone=data['resource_zone'],
                             resource_hostname=data['resource_name'],
                             timestamp=unify_timestamp(data['timestamp']),
                             eventType=data['eventType'],
                             metric=data['metric'],
                             value=data['value'])
      except:
        logging.error('Incorrect log value')
        logging.error(sys.exc_info()[0])
        raise
      ttriggered = False
      ttriggered = env.agenda_changed
      logging.info("Get logs from monitoring... OK")
      logging.info(f"""Transition triggered... {ttriggered}""")
      #https://clipspy.readthedocs.io/en/latest/clips.html#module-clips.environment
      for act in env.activations():
        if ttriggered:
          # Temp the triggered transition according to the time defined in the rules
          act.delete()
          print(act.name)
          print(transitions[act.name])
          if transitions[act.name][0] == 'ResourceRelatedEvent':
            if act.name in Ttriggered_log:
              ## If the transition has already been triggered compare
              ## the timestamp of the first occurence with the actual
              firstStamp = datetime.strptime(Ttriggered_log[act.name], "%Y-%m-%dT%H:%M:%S.%fZ")
              actualStamp =  datetime.strptime(data['timestamp'], "%Y-%m-%dT%H:%M:%S.%fZ")
              diff = actualStamp - firstStamp

              if diff.seconds >= transitions[act.name][2]['time']:
                send_triggered_transitions(channel, act.name)
                Ttriggered_log.pop(act.name)
            else:
              Ttriggered_log[act.name] = data['timestamp']
            # Notify Deployment when event is triggered
          elif transitions[act.name][0] == 'TemporalEvent':
            send_triggered_transitions(channel, act.name)
    time.sleep(4)

if __name__ == '__main__':
  try:
    main()
  except KeyboardInterrupt:
    logging.info('Interrupted')
    try:
      sys.exit(0)
    except SystemExit:
      os._exit(0)
