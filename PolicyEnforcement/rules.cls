=: Equal (numeric type). Compares magnitude.
<>: Not equal (numeric type).
>=: Greater than or equal to
>: Greater than.
<=: Less than or equal to.
<: Less than.



;Exemple CSM
;rule "T1 Q-Event"
;    when
;       $currentState: CurrentState(S == "S1")
;       $cooldown: Cooldown(C == true)
;       Q_Event
;       (
;       metric == "cpuusage" && 
;       operator == ">=" && 
;       (float)valueMetric >= (float)threshold
;       && $cooldown.cooldownDate <= captureDate
;       )
;    then
;        System.out.println("\t\tExecute Action T1");
;        Horizontal_Scaling.Terraform_VM("scale-out","AWS","change-in-capacity",2);
;		modify ( $currentState ) { setS("S2") }; 
;		modify ( $cooldown ) { waitCooldown(120) };
;end