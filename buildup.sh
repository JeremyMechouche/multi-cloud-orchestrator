#!/bin/sh
timedatectl set-timezone Europe/Paris
docker build -t opensla/adaptvalidation AdaptationValidation/
docker build -t opensla/policyenforcement PolicyEnforcement/
docker build -t opensla/monitoring Monitoring/
docker build -t opensla/deployment Deployment/

docker pull docker.elastic.co/beats/filebeat:7.15.1
docker pull docker.elastic.co/beats/metricbeat:7.15.1
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.15.1
