import csv, json
import numpy as np
import pandas
from elasticsearch import Elasticsearch
from datetime import datetime

# Structure du format 
## "source" : "", # Agent | CSP | Orchestrateur
## "case" : "", # A récupérer via le tag de la ressource
## "state" : ""
## "resource_provider" : ""
## "resource_hostname" : ""
## "resource_zone" : ""
## "timestamp" : ""
## "eventType" : ""
## "metric" : ""
## "value" : ""

class Monitor:
    def __init__(self):
        # Authentification to ELK
        self.client = Elasticsearch(
            '172.16.138.103',
            #http_auth=('elastic','changeme'),
            scheme='http',
            port=9200)

    def write_data(self):
        payload = {
            "source" : "orchestrator",
            "case" : "",
            "state" : "",
            "resource_provider" : "",
            "resource_hostname" : "",
            "ressource_zone" : "",
            "timestamp": datetime.now(),
            "eventType": "",
            "metric": "",
            "value": ""
        }
        print(self.client.index(index="orchestrator", id=1, body=payload))

    def get_resource_log(self):
        response = self.client.search(
            index="metricbeat-*", 
            body={
                "query":{
                    "range":{
                        "@timestamp":{
                            "gte":"2021-06-01T13:00:19.443Z", # "2021-06-01T13:00:19.443Z"
                            "lte":datetime.now(), # "2021-07-06T15:34:03.950Z"
                        }
                    }
                }
            },
            size=1000
        )
        elastic_docs = response["hits"]["hits"]
        docs = pandas.DataFrame()

        for num, doc in enumerate(elastic_docs):
            #print(doc["_source"])
            source_data = doc["_source"]
            source_data = {
                "source" : "Agent", # Agent | CSP | Orchestrateur
                "case" : "Case1", # A récupérer via le tag de la ressource
                "state" : "S1", # A récuperer via le tag de la ressource
                "resource_provider" : doc["_source"]["cloud"]["provider"],
                "resource_hostname" : doc["_source"]["agent"]["hostname"],
                "resource_zone" : doc["_source"]["cloud"]["availability_zone"],
                "timestamp" : doc["_source"]["@timestamp"]
            }
            if "system" in doc["_source"]:
                if "cpu" in doc["_source"]["system"]:
                    source_data["eventType"] = "Q-Event"
                    source_data["metric"] = "cpuusage"
                    source_data["value"] = doc["_source"]["system"]["cpu"]
                elif "network" in doc["_source"]["system"]:
                    source_data["eventType"] = "Q-Event"
                    source_data["metric"] = "networkusage"
                    source_data["value"] = doc["_source"]["system"]["network"]
                elif "socket" in doc["_source"]["system"]:
                    source_data["eventType"] = "Q-Event"
                    source_data["metric"] = "networksocket"
                    source_data["value"] = doc["_source"]["system"]["socket"]["summary"]["all"]
                elif "process" in doc["_source"]["system"]:
                    if "summary" in doc["_source"]["system"]["process"]:
                        source_data["eventType"] = "Q-Event"
                        source_data["metric"] = "processNb"
                        source_data["value"] = doc["_source"]["system"]["process"]["summary"]
                    else:
                        print(doc["_source"])
            elif "host" in doc["_source"]:
                if "network" in doc["_source"]["host"]:
                    source_data["eventType"] = "Q-Event"
                    source_data["metric"] = "networkusage"
                    source_data["value"] = doc["_source"]["host"]["network"]
            else:
                pass
                #print(doc["_source"])

            _id = doc["_id"]
            doc_data = pandas.Series(source_data, name = '')
            docs = docs.append(doc_data)

        docs.to_csv("ResourceLog.csv", ",")

    def get_csp_log(self):
        response = self.client.search(
            index="filebeat-*", 
            body={
                "query":{
                    "range":{
                        "@timestamp":{
                            "gte":"2021-07-12T08:00:19.443Z", # "2021-06-01T13:00:19.443Z"
                            "lte":datetime.now(), # "2021-07-06T15:34:03.950Z"
                        }
                    }
                }
            },
            size=1000
        )
        elastic_docs = response["hits"]["hits"]
        docs = pandas.DataFrame()
        for num, doc in enumerate(elastic_docs):
            #print(doc["_source"])
            source_data = {
                "source" : "CSP", # Agent | CSP | Orchestrateur
                "case" : "Case1", # A récupérer via le tag de la ressource
                "state" : "S1", # A récuperer via le tag de la ressource
                "resource_provider" : "GCP",
                "timestamp" : doc["_source"]["@timestamp"]
                #"metric": "",
                #"value": ""
            }
            if "proto_name" in doc["_source"]['gcp']['audit']['request']:
                if doc["_source"]['gcp']['audit']['request']['proto_name'] == "compute.instances.insert":
                    # Create
                    source_data["resource_hostname"] = doc["_source"]['gcp']['audit']['resource_name'],
                    #source_data["resource_zone"] = doc["_source"]['gcp']['audit']['resource_location']['current_locations'][0],
                    source_data["eventType"] = "Create_VM"
                elif doc["_source"]['gcp']['audit']['request']['proto_name'] == "type.googleapis.com/compute.instances.stop":
                    # Stop
                    source_data["resource_hostname"] = doc["_source"]['gcp']['audit']['resource_name'],
                    #source_data["resource_zone"] = doc["_source"]['gcp']['audit']['resource_location']['current_locations'][0],
                    source_data["eventType"] = "Stop_VM"
                elif doc["_source"]['gcp']['audit']['request']['proto_name'] == "type.googleapis.com/compute.instances.start":
                    # Start 
                    source_data["resource_hostname"] = doc["_source"]['gcp']['audit']['resource_name'],
                    #source_data["resource_zone"] = doc["_source"]['gcp']['audit']['resource_location']['current_locations'][0],
                    source_data["eventType"] = "Start_VM"
                elif doc["_source"]['gcp']['audit']['request']['proto_name'] == "type.googleapis.com/compute.instances.delete":
                    # Delete
                    source_data["resource_hostname"] = doc["_source"]['gcp']['audit']['resource_name'],
                    #source_data["resource_zone"] = doc["_source"]['gcp']['audit']['resource_location']['current_locations'][0],
                    source_data["eventType"] = "Delete_VM"
                _id = doc["_id"]
                doc_data = pandas.Series(source_data, name = '')
                docs = docs.append(doc_data)
        docs.to_csv("CSPLog.csv", ",")

m = Monitor()
#m.get_csp_log()
