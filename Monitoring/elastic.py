from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from datetime import datetime
import random, string

def generate_random_id():
    """ 
        generate ids for metrics
        mix de 20 caracteres majuscule, minuscule et chiffres 
    """ 
    characters = string.ascii_letters + string.digits
    id = ''.join(random.choice(characters) for i in range(20))
    return id

# Connection to elasticSearch
def connect(ip, id, passwd, port):
    """
        Connection function to elasticsearch base
        input
            ip, id, passwd, port
        output
            elasticsearch client
    """
    return Elasticsearch(
        ip,
        http_auth=(id, passwd),
        sheme='http',
        port=port
    )

# Write data from Orchestrator
def write_data(case, state, res_prov, res_host, res_zone, evt_type, metric, value):
    """
        Write data for orchestration action
    """
    client = connect('34.79.102.171', 'elastic', 'changeme', 8080)
    payload = {
        "source" : "orchestrator",
        "case" : case,
        "state" : state,
        "resource_provider" : res_prov,
        "resource_hostname" : res_host,
        "ressource_zone" : res_zone,
        "@timestamp" : datetime.now(),
        "eventType" : evt_type,
        "metric" : metric,
        "value" : value
    }
    res = client.index(index="orchestrator", id=generate_random_id(), body=payload)
    client.indices.refresh(index="orchestrator")
    return res
