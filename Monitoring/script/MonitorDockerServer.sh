ELK_IP=10.0.2.15

docker network create elastic
docker run --name es01 -d --net elastic -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.15.0 
#docker run --name beats01 -d --net elastic --mount type=bind,source=/proc,target=/hostfs/proc,readonly --mount type=bind,source=/sys/fs/cgroup,target=/hostfs/sys/fs/cgroup,readonly --mount type=bind,source=/,target=/hostfs,readonly --net=host docker.elastic.co/beats/metricbeat:7.15.0 -e -system.hostfs=/hostfs -E output.elasticsearch.hosts=[$ELK_IP":9200"]

docker run --name kib01 -d --net elastic -p 5601:5601 -e "ELASTICSEARCH_HOSTS=http://10.0.2.15:9200" docker.elastic.co/kibana/kibana:7.15.0
docker run -d --name=filebeat --net elastic --volume="$(pwd)/filebeat.docker.yml:/usr/share/filebeat/filebeat.yml:ro" docker.elastic.co/beats/filebeat:7.15.0 filebeat -e -strict.perms=false   -E output.elasticsearch.hosts=["10.0.2.15:9200"]

#Check 
#curl -XGET http://127.0.0.1:9200