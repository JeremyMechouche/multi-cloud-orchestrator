"""
    Monitoring Module
"""

import csv, json, os, requests, time
import numpy as np
import pandas, time, sys, logging
import pika
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from datetime import datetime

IP_RABBITMQ = '10.0.2.15'
IP_ELASTIC = '10.0.2.15'

# Connection to Rabbitmq
def connection_mq(ip):
  connection = pika.BlockingConnection(pika.ConnectionParameters(ip))
  return connection.channel()

def emit_data_mq(channel, queue, message):
  channel.queue_declare(queue=queue)
  channel.basic_publish(exchange='',
                        routing_key=queue,
                        body=message)
  logging.info(f""" [x] Sent {message}""")

def GET_current_State(es):
    res = es.get(index="orchestrator-related", id='plan')
    return res['_source']['state']['type']

# Connection to elasticSearch
def connect(ip, id, passwd, port):
    """
        Connection function to elasticsearch base
        input
            ip, id, passwd, port
        output
            elasticsearch client
    """
    return Elasticsearch(
        ip,
        http_auth=(id, passwd),
        sheme='http',
        port=port
    )

# Create filter to get data from elastic
def read_data(client, index, body):
    """
        Generic function to read data from elastic search base
    """
    response = client.search(
        index=index, 
        body=body
    )
    client.indices.refresh(index=index)
    #print(response)
    response = response["hits"]["hits"]
    return response

def get_resource_log(client):
        response = client.search(
            index="metricbeat-*", 
            body={
                "query":{
                    "range":{
                        "@timestamp":{
                            "gte":"2021-08-01T00:00:00.000Z", # "2021-06-01T13:00:19.443Z"
                            "lte":datetime.now(), # "2021-07-06T15:34:03.950Z"
                        }
                    }
                }
            },
            size=1000
        )
        elastic_docs = response["hits"]["hits"]
        docs = pandas.DataFrame()

        for num, doc in enumerate(elastic_docs):
            #print(doc["_source"])
            source_data = doc["_source"]
            source_data = {
                "source" : "Agent", # Agent | CSP | Orchestrateur
                "case" : "Case1", # A récupérer via le tag de la ressource
                "state" : "S1", # A récuperer via le tag de la ressource
                "resource_provider" : doc["_source"]["cloud"]["provider"],
                "resource_hostname" : doc["_source"]["agent"]["hostname"],
                "resource_zone" : doc["_source"]["cloud"]["availability_zone"],
                "timestamp" : doc["_source"]["@timestamp"]
            }
            if "system" in doc["_source"]:
                if "cpu" in doc["_source"]["system"]:
                    source_data["eventType"] = "Q-Event"
                    source_data["metric"] = "cpuusage"
                    source_data["value"] = doc["_source"]["system"]["cpu"]
                elif "network" in doc["_source"]["system"]:
                    source_data["eventType"] = "Q-Event"
                    source_data["metric"] = "networkusage"
                    source_data["value"] = doc["_source"]["system"]["network"]
                elif "socket" in doc["_source"]["system"]:
                    source_data["eventType"] = "Q-Event"
                    source_data["metric"] = "networksocket"
                    source_data["value"] = doc["_source"]["system"]["socket"]["summary"]["all"]
                elif "process" in doc["_source"]["system"]:
                    if "summary" in doc["_source"]["system"]["process"]:
                        source_data["eventType"] = "Q-Event"
                        source_data["metric"] = "processNb"
                        source_data["value"] = doc["_source"]["system"]["process"]["summary"]
                    else:
                        print(doc["_source"])
            elif "host" in doc["_source"]:
                if "network" in doc["_source"]["host"]:
                    source_data["eventType"] = "Q-Event"
                    source_data["metric"] = "networkusage"
                    source_data["value"] = doc["_source"]["host"]["network"]
            else:
                pass
                #print(doc["_source"])

            _id = doc["_id"]
            doc_data = pandas.Series(source_data, name = '')
            docs = docs.append(doc_data)

        docs.to_csv("ResourceLog.csv", ",")

# Get objectives to monitor from the SLA
def GET_sla(es):
    res = es.get(index="orchestrator-related", id='sla')
    return res['_source']

def GET_Objectives(es):
    return GET_sla(es)['Objectives']

def GET_Events(es):
    """ 
        Function returning unique events in transitions
    """
    output = []
    sla = GET_sla(es)
    for transition in sla['StateMachine']['Transitions']:
        id = transition['events'][0]['id']
        type = transition['events'][0]['type']
        predicate = transition['events'][0]['predicate']
        event = [id, type, predicate]
        if event not in output:
            output.append([id, type, predicate])
    return output

def GET_Resources(es):
    """
        Return list of resources
    """
    output = []
    sla = GET_sla(es)
    for resource in sla['ServiceCover']['resources']:
        output.append(resource)
    return output

# get data from the three sources
def get_orchestrator_data(client):
    request = {
        "query":{
            "bool":{
                "must":[
                    #{"match": {"_id": "dtZRe5TiFCAuzLsg0tz1"}},
                    #{"match": {'metric': 'cpuusage'}}
                ],
                "filter":[
                    {"range": {"@timestamp":{
                        "gte":"2021-08-01T00:00:00.000Z",
                        "lte":datetime.now()
                    }}}
                ]
            }
        }
    }
    res = read_data(client, "orchestrator", request)
    return res

def get_csp_data(client):
    request = {
        "query":{
            "bool":{
                "must":[
                    #{"match": {"_id": "dtZRe5TiFCAuzLsg0tz1"}},
                    #{"match": {'metric': 'cpuusage'}}
                ],
                "filter":[
                    {"range": {"@timestamp":{
                        "gte":"2021-08-01T00:00:00.000Z",
                        "lte":datetime.now()
                    }}}
                ]
            }
        }
    }
    res = read_data(client, "filebeat-*", request)
    return res

def get_resource_data(client, service):
    request = {
        "query":{
            "bool":{
                "must":[
                    {"match": {'docker.container.labels.com_docker_swarm_service_name': service}},
                    {"exists": {"field":'docker.cpu.total.pct'}}]
                #,"filter":[
                #    {"range": {"@timestamp":{"gte":"2021-12-20T21:00:00.000Z","lte":datetime.now()}}}]
            }
        },
        "fields": [
            "@timestamp",
            'docker.container.labels.com_docker_swarm_service_name',
            'docker.container.labels.state',
            "docker.cpu.total.pct"
        ],
        "_source": 'false',
        "sort": [{"@timestamp": {"order": "desc"}}],
        "size": 1
    }
    res = read_data(client, "metricbeat-*", request)
    return res

def filtered_resource_data(client):
    ret = get_resource_data(client)
    docs = pandas.DataFrame()
    for num, doc in enumerate(ret):
        #source_data = {} # 
        #print(doc["_source"])
        source_data = {
            "source" : "Agent",
            "case": "",
            "state": "",
            "resource_provider" : doc["_source"]["cloud"]["provider"],
            "resource_hostname" : doc["_source"]["agent"]["hostname"],
            "resource_zone" : doc["_source"]["cloud"]["availability_zone"],
            "timestamp" : doc["_source"]["@timestamp"],
            "eventType" : "",
            "metric" : "",
            "value" : ""
        }
        if "system" in doc["_source"]:
            if "cpu" in doc["_source"]["system"]:
                source_data["eventType"] = "Q-Event"
                source_data["metric"] = "cpuusage"
                source_data["value"] = doc["_source"]["system"]["cpu"]
            elif "network" in doc["_source"]["system"]:
                source_data["eventType"] = "Q-Event"
                source_data["metric"] = "networkusage"
                source_data["value"] = doc["_source"]["system"]["network"]
            elif "socket" in doc["_source"]["system"]:
                source_data["eventType"] = "Q-Event"
                source_data["metric"] = "networksocket"
                source_data["value"] = doc["_source"]["system"]["socket"]["summary"]["all"]
            elif "process" in doc["_source"]["system"]:
                if "summary" in doc["_source"]["system"]["process"]:
                    source_data["eventType"] = "Q-Event"
                    source_data["metric"] = "processNb"
                    source_data["value"] = doc["_source"]["system"]["process"]["summary"]
                else:
                    pass
                    #print(doc["_source"])
        elif "host" in doc["_source"]:
            if "network" in doc["_source"]["host"]:
                source_data["eventType"] = "Q-Event"
                source_data["metric"] = "networkusage"
                source_data["value"] = doc["_source"]["host"]["network"]
        else:
            pass
            #print(doc["_source"])
        print(source_data)
        _id = doc["_id"]
        doc_data = pandas.Series(source_data, name='')
        docs = docs.append(doc_data)
    docs.to_csv("ResourceLog.csv",",")

def filtered_csp_data(client):
    ret = get_csp_data
    docs = pandas.DataFrame()
    for num, doc in enumerate(ret):
        #source_data = {} # 
        #print(doc["_source"])
        source_data = {
            "source" : "Agent",
            "case": "",
            "state": "",
            "resource_provider" : doc["_source"]["cloud"]["provider"],
            "resource_hostname" : doc["_source"]["agent"]["hostname"],
            "resource_zone" : doc["_source"]["cloud"]["availability_zone"],
            "timestamp" : doc["_source"]["@timestamp"],
            "eventType" : "",
            "metric" : "",
            "value" : ""
        }
        if "system" in doc["_source"]:
            if "cpu" in doc["_source"]["system"]:
                source_data["eventType"] = "Q-Event"
                source_data["metric"] = "cpuusage"
                source_data["value"] = doc["_source"]["system"]["cpu"]
            elif "network" in doc["_source"]["system"]:
                source_data["eventType"] = "Q-Event"
                source_data["metric"] = "networkusage"
                source_data["value"] = doc["_source"]["system"]["network"]
            elif "socket" in doc["_source"]["system"]:
                source_data["eventType"] = "Q-Event"
                source_data["metric"] = "networksocket"
                source_data["value"] = doc["_source"]["system"]["socket"]["summary"]["all"]
            elif "process" in doc["_source"]["system"]:
                if "summary" in doc["_source"]["system"]["process"]:
                    source_data["eventType"] = "Q-Event"
                    source_data["metric"] = "processNb"
                    source_data["value"] = doc["_source"]["system"]["process"]["summary"]
                else:
                    pass
                    #print(doc["_source"])
        elif "host" in doc["_source"]:
            if "network" in doc["_source"]["host"]:
                source_data["eventType"] = "Q-Event"
                source_data["metric"] = "networkusage"
                source_data["value"] = doc["_source"]["host"]["network"]
        else:
            pass
            #print(doc["_source"])
        print(source_data)
        _id = doc["_id"]
        doc_data = pandas.Series(source_data, name='')
        docs = docs.append(doc_data)
    docs.to_csv("ResourceLog.csv",",")

def connect_elasticsearch():
    connected = False
    _es = Elasticsearch([{'host': '10.0.2.15', 'port':'9200'}])
    try:
        if _es.ping():
            print('Connected!')
            connected = True
    except Exception as ex:
        print(str(ex))
    finally:
        return connected, _es

def search(es, index_name, search):
    res = es.search(index=index_name, body=search)
    return res

def get_monitoring_data(es):
    body_item = {
        '_source': 'event.*',
        #{"match_all": {}},
        "query":{
            "term": {'dataset':'docker.cpu'}
        },
        "size": 150,
        "sort": [
            {
                "@timestamp": {
                "order": "desc"
                }
            }
        ]
    }
    res = search(es, index_name='metricbeat-*', search=body_item)
    return res

def main():
    #### Connect to Elasticsearch
    logging.basicConfig(level=logging.INFO)
    logging.info("Connection to elasticsearch...")
    connected, es = connect_elasticsearch()
    #### Connect to Rabbitmq
    channel = connection_mq(IP_RABBITMQ)
    
    if connected == True:
        logging.info("Connected to elasticsearch!")
        #### Retrieving objectives to monitor
        Objectives = GET_Objectives(es)
        #### Transitions to Monitor
        print("############## Events ##############")
        Events = GET_Events(es)
        #### Start Monitoring these objectives from the three log sources
        ### Check every 2 seconds for each resources in service cover
        Resources = GET_Resources(es)
        while GET_current_State(es) != 'Final':
            for resource in Resources:
                data = get_resource_data(es,resource['id'])
                print(data)
                if data != []:
                    message = {}
                    message['source'] = 'resource'
                    message['case'] = 'Case1'
                    message['state'] = data[0]['fields']['docker.container.labels.state'][0]
                    message['resource_name'] = resource['id']
                    message['resource_provider'] = 'Docker'
                    message['resource_zone'] = 'Paris'
                    message['timestamp'] = data[0]['fields']['@timestamp'][0]
                    message['eventType'] = 'Q-Event'
                    message['metric'] = 'cpuusage'
                    message['value'] = data[0]['fields']['docker.cpu.total.pct'][0] * 100
                    emit_data_mq(channel, 'log', json.dumps(message))
            time.sleep(4)
        print("Final state reached ...")
    else:
        print('Connection failed...')

if __name__ == '__main__':
  try:
    main()
  except KeyboardInterrupt:
    logging.info('Interrupted')
    try:
      sys.exit(0)
    except SystemExit:
      os._exit(0)
      