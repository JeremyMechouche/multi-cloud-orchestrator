""" 
    CLI to interact with the orchestrator
"""
from cmd import Cmd

class Prompt(Cmd):
    prompt = 'pb> '
    intro = 'Hello There :'
    def do_exit(self, inp):
        '''exit the application'''
        print("Bye")
        return True
    
    def do_add(self, inp):
        print("Adding '{}'".format(inp))
    
    def do_start(self, inp):
        pass
