import os
#from provider import Docker

class CSM:
    """
        Cloud State Machine 
    """
    def __init__(self, id):
        self.id = id
        self.transitions = []
        self.states = []
        self.resources = {}
        self.actual_state = None
        self.final_state = False

    #def _enforce_resource(self, resources):
    #    """
    #        Create resources for the first state
    #    """
    #    for resource in self.resources:
    #        if self.resources[resource].attributes['provider'] == 'Docker':
    #            res = Docker.cont()
    #            res.create(self.resources[resource].id, self.resources[resource].attributes['image'], self.resources[resource].attributes['number'], self.actual_state)
    #            self.resources[resource].connect = res 

    #def _update_resource(self, requirements, state):
    #    """
    #        Update resource when a transition occur
    #    """
    #    for resource in requirements:          
    #        for attributes in resource:
    #            res = self.resources[attributes].connect
    #            res.scale(resource[attributes]['number'], state)

    #def _delete_resource(self,state):
    #    """
    #        Delete resources
    #    """
    #    for resource in self.resources:
    #        res = self.resources[resource].connect
    #        res.delete(state)

    def start(self):
        """
            Start the state-machine
        """
        for state in self.states:
            if state.type == "Initial":
                self.actual_state = state.id
                #self._enforce_resource(state.requirement)

    def change_state(self, transition_ID):
        transition_to_trigger = None
        for transition in self.transitions:
            if (transition.id == transition_ID) and (transition.source == self.actual_state):
                transition_to_trigger = transition
        if transition_to_trigger == None:
            print("Transition doesn't exist or is incorrect for the current state!")
        else:
            print(f"""Transition ok from {transition_to_trigger.source} to {transition_to_trigger.target}""")
            self.actual_state = transition_to_trigger.target
            for state in self.states:
                if state.id == self.actual_state and state.type == 'Final':
                    self.final_state = True
                    print(f"""State Machine stopped at state {self.actual_state}""")
                    #self._delete_resource(self.actual_state)
                #elif state.id == self.actual_state:
                #    self._update_resource(state.requirement, self.actual_state)

class State:
    def __init__(self, id, type, requirements):
        self.id = id
        self.type = type
        self.requirement = requirements

class Transition:
    def __init__(self, id, source, target, event, action):
        self.id = id
        self.source = source
        self.target = target
        self.event = event
        self.action = action

class ResourceRequirement:
    def __init__(self, id, name, attributes):
        self.id = id
        self.name = name
        self.attributes = attributes
        self.connect = None

class Event:
    def __init__(self, id, type, predicate, expresion):
        self.id = id
        self.type = type
        self.predicate = predicate
        self.expression = expresion

class ReconfigurationAction:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.actionAttribute = [None]

class ActionAttribute:
    def __init__(self, name, value):
        self.name = name
        self.value = value


