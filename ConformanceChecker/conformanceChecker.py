"""
    Conformance checker module 
"""
import pm4py
import pandas as pd
import os
import yaml, json

from pm4py.objects.log.util import dataframe_utils
from pm4py.objects.log.exporter.xes import exporter as xes_exporter
from pm4py.objects.conversion.log import converter as log_converter
from pm4py.algo.conformance.alignments.petri_net import algorithm as alignments

from pm4py.objects.petri_net.importer import importer as pnml_importer
from pm4py.objects.petri_net.obj import PetriNet, Marking
from pm4py.objects.petri_net.utils.petri_utils import add_arc_from_to

from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from datetime import datetime


IP_ELASTIC = '10.0.2.15'

class sla: 
    """
        Read Multi-Cloud SLA file
    """
    def __init__(self, file):
        with open(file, 'r') as stream:
            try:
                self.SLA = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
    def validate(self):
        pass

def model_translator(es):
    SLA = GET_sla(es)
    Current_CSM = CSM(SLA['name'])
    for resource in SLA['ServiceCover']['resources']:
        Current_CSM.resources[resource['id']] = ResourceRequirement(resource['id'], 
                                                                    resource['description'], 
                                                                    resource['attributes'])
    for state in SLA['StateMachine']['States']:
        Current_CSM.states.append(
            State(state['id'],
            state['type'],
            state['resources']
        ))
    for transition in SLA['StateMachine']['Transitions']:
        Current_CSM.transitions.append(
            Transition(transition['id'], 
            transition['stateSource'], 
            transition['stateTarget'], 
            transition['events'], 
            transition['actions']
        ))
    return SLA

##### ElasticSearch ###### 
def CONNECT_elasticsearch(IP):
    connected = False
    _es = Elasticsearch([{'host': IP, 'port':'9200'}])
    try:
        if _es.ping():
            print('Connected!')
            connected = True
    except Exception as ex:
        print(str(ex))
    finally:
        return connected, _es

def get(es, index_name, id_name):
    res = es.get(index=index_name, id=id_name)
    return res

def read_data(client, index, body):
    """
        Generic function to read data from elastic search base
    """
    response = client.search(
        index=index, 
        body=body,
        size=10000
    )
    client.indices.refresh(index=index)
    #print(response)
    response = response["hits"]["hits"]
    return response

def GET_CSM(es):
    sla = GET_sla(es)
    res = sla['StateMachine']
    return res

def GET_sla(es):
    res = es.get(index="orchestrator-related", id='sla')
    return res['_source']

def GET_Logs(es, begin_timestamp, end_timestamp):
    ## Request from Elasticsearch
    begin_timestamp = begin_timestamp[:10] + "T" + begin_timestamp[11:] + "Z"
    end_timestamp = end_timestamp[:10] + "T" + end_timestamp[11:] + "Z"
    request_resource_data = {
        "query":{
            "bool":{
                "must":[
                    {"exists": {"field":"docker.container.labels.com_docker_swarm_service_name"}},
                    {"exists": {"field":'docker.cpu.total.pct'}}
                ],
                "filter":[{
                    "range": {
                        "@timestamp":{
                            "gte":begin_timestamp,
                            "lte":end_timestamp
                        }
                    }
                }]
            }
        },
        "fields": [
            "@timestamp",
            'docker.container.labels.com_docker_swarm_service_name',
            'docker.container.labels.state',
            "docker.cpu.total.pct"
        ],
        "_source": 'false',
        "sort": [{"@timestamp": {"order": "desc"}}]
    }
    Logs_resource = read_data(es,"metricbeat-*", request_resource_data)
    request_res_provider = {
        "query":{
            "bool":{
                "filter":[
                    {"range": {"@timestamp":{
                        "gte":begin_timestamp,
                        "lte":datetime.now()  #end_timestamp
                    }}}
                ]
            }
        }
    }
    Logs_provider = read_data(es,"filebeat-*", request_res_provider)

    ### Converted to Dataframe
    Logs_resource_pd = pd.DataFrame.from_dict([log['fields'] for log in Logs_resource])
    Logs_provider_pd = pd.DataFrame.from_dict([log['_source']['message'] for log in Logs_provider])

    ### Merge logs 
    Logs = pd.concat([Logs_provider_pd, Logs_resource_pd])

    return Logs

#### Conformance Checker
## Model Translator
def convert_to_PNML(sla):
    """
        Convert the state Machine define in the Multi-cloud SLA in a Data Petri Net 
    """ 
    # DPN Instanciation 
    DPN = PetriNet(sla['name'])
    s = sla['StateMachine']
    # States creation in DPN
    for state in s['States']:
        DPN.places.add(PetriNet.Place(state['id']))
        if state['type'] == 'Initial':
            im = Marking()
            im[PetriNet.Place(state['id'])] = 1
        elif state['type'] == 'Final':
            fm = Marking()
            fm[PetriNet.Place(state['id'])] = 1
    # Transitions and arc creation in DPN
    for transition in s['Transitions']:
        predicate = transition['events'][0]['predicate']
        guard = ''
        if transition['events'][0]['type'] == 'ResourceRelatedEvent':
            guard = predicate['metric'] + predicate['operator'] + str(predicate['refValue'])
        if transition['events'][0]['type'] == 'TemporalEvent':
            guard = "timestamp==" + predicate['timestamp']
        DPN.transitions.add(PetriNet.Transition(transition['id'], properties={'guard':guard}))
        add_arc_from_to(PetriNet.Place(transition['stateSource']),PetriNet.Transition(transition['id']), DPN)
        add_arc_from_to(PetriNet.Transition(transition['id']), PetriNet.Place(transition['stateTarget']), DPN)
    return DPN, im, fm

def convert_to_XES(log):
    """
        Convert event logs from CSV into XES format 
    """
    log_csv = pd.read_csv(log)
    log_csv = dataframe_utils.convert_timestamp_columns_in_df(log_csv)
    log_csv = log_csv.sort_values('Timestamp')
    event_log = log_converter.apply(log_csv)
    log_xes = xes_exporter.apply(event_log,'./log.xes')
    return log_xes

def convert_pd_to_XES(log):
    """
        Convert event logs from Pandas Dataframe into XES format 
    """
    log_pd = dataframe_utils.convert_timestamp_columns_in_df(log)
    log_pd = log_pd.sort_values('@timestamp')
    event_log = log_converter.apply(log_pd)
    log_xes = xes_exporter.apply(event_log,'./log.xes')
    return log_xes

## Conformance Checker
def conformance_checking(log, net, im, fm):
    #log = pm4py.read_xes("Exemple1SLA.xes")
    #net, im, fm = pnml_importer.apply("NormalNeeds.pnml")
    # Value definition for cost function
    #model_cost_function = dict()
    #sync_cost_function = dict()
    #for t in net.transitions:
    #    if t.label is not None:
    #        # transition cost function
    #        model_cost_function[t] = 1000
    #        sync_cost_function[t] = 0
    #    else:
    #        # invisible move
    #        model_cost_function[t] = 1
    #parameters = {}
    #parameters[alignments.Variants.VERSION_STATE_EQUATION_A_STAR.value.Parameters.PARAM_MODEL_COST_FUNCTION] = model_cost_function
    #parameters[alignments.Variants.VERSION_STATE_EQUATION_A_STAR.value.Parameters.PARAM_SYNC_COST_FUNCTION] = sync_cost_function
    #parameters["ret_tuple_as_trans_desc"] = True

    aligned_traces = alignments.apply(log, net, im, fm) #, parameters=parameters)
    
    for trace in aligned_traces:
        print(trace)
        print("########")

## Multi-perspective conformance checker
def MP_conformance_checking(log_file, pnet_file):
    # TEMP import net, initial and final marking
    net, im, fm = pm4py.read_pnml(os.path.join("ProcessMining", pnet_file))
    # TEMP import logs 
    log = pm4py.read_xes(os.path.join("ProcessMining", log_file))
    # Definition de la fonction de coût
    K = ''
    # export place from marking 
    # TODO: Find a better way to do this
    for key in im.keys(): im = key
    for key in fm.keys(): fm = key
    # Algorithm of Multi Perspective conformance checking
    aligned_traces = balancedConformance(net, im, fm, log)

if __name__ == "__main__":
    ## Connection to Elastic Search
    connected, es = CONNECT_elasticsearch(IP_ELASTIC)
    if connected == True: 
        ## GET SLA
        SLA = GET_sla(es)

        ## GET logs
        Logs = GET_Logs(es, SLA['validityPeriod']['start'], SLA['validityPeriod']['end'])

        ## Model Translator
        net, im, fm = convert_to_PNML(SLA)

        ## Log Transformer
        Logs_xes = convert_pd_to_XES(Logs)

        conformance_checking(Logs_xes, net, im, fm)
