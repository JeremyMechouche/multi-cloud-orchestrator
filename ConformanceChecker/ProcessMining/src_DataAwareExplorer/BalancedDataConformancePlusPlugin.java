/*
 * Copyright (c) 2014 F. Mannhardt (f.mannhardt@tue.nl)
 */
package org.processmining.plugins.balancedconformance;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.datapetrinets.DataPetriNet;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.Progress;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginCategory;
import org.processmining.framework.plugin.annotations.PluginLevel;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.framework.util.ui.widgets.helper.ProMUIHelper;
import org.processmining.framework.util.ui.widgets.helper.UserCancelledException;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetGraph;
import org.processmining.plugins.balancedconformance.config.BalancedProcessorConfiguration;
import org.processmining.plugins.balancedconformance.config.DataConformancePlusConfiguration;
import org.processmining.plugins.balancedconformance.controlflow.ControlFlowAlignmentException;
import org.processmining.plugins.balancedconformance.dataflow.exception.DataAlignmentException;
import org.processmining.plugins.balancedconformance.observer.DataConformancePlusObserverImpl;
import org.processmining.plugins.balancedconformance.result.BalancedDataAlignmentState;
import org.processmining.plugins.balancedconformance.result.BalancedReplayResult;
import org.processmining.plugins.balancedconformance.result.StatisticResult;
import org.processmining.plugins.utils.ProvidedObjectHelper;

import com.google.common.util.concurrent.AtomicDouble;

/**
 * Alignment of an event log {@link XLog} and a Petri net with Data
 * {@link DataPetriNet}, as described in
 * <a href="http://dx.doi.org/10.1007/s00607-015-0441-1" >10.1007/s00607-015-
 * 0441-1</a>.
 * <p>
 * Use the method
 * {@link #doBalancedAlignmentDataConformanceChecking(DataPetriNet, XLog, Progress, BalancedProcessorConfiguration)}
 * to get the alignment and the fitness for each trace in form of a
 * {@link BalancedReplayResult}.
 * <p>
 * To get the configuration {@link BalancedProcessorConfiguration}, either
 * create it from hand or use #queryConfiguration(UIPluginContext,
 * PetriNetWithData, XLog)} to query from the user. You may also use the static
 * method
 * {@link BalancedProcessorConfiguration#newDefaultInstance(DataPetriNet, org.processmining.models.semantics.petrinet.Marking, org.processmining.models.semantics.petrinet.Marking[], XLog, XEventClassifier, int, int, int, int)}
 * to create a sensible default configuration.
 * <p>
 * Each trace is aligned using an instance of the class
 * {@link DataAlignmentTraceProcessor}. You can also use
 * {@link DataAlignmentTraceProcessor#DataAlignmentTraceProcessor(XTrace, DataPetriNet, BalancedProcessorConfiguration)}
 * to align a single trace only.
 * 
 * @author F. Mannhardt
 * 
 */
@Plugin(name = "Conformance Checking of DPN", level = PluginLevel.PeerReviewed, parameterLabels = { "Data petri net",
		"Log", "Configuration" }, returnLabels = { "Alignment Result" }, returnTypes = {
				BalancedReplayResult.class }, userAccessible = true, handlesCancel = true, categories = PluginCategory.ConformanceChecking, keywords = {
						"conformance", "DPN", "data", "alignment" }, //
		help = "Conformance Checking of a Petri net with Data with regard to an event log."
				+ "The technique is described in the article 'Balanced multi-perspective checking of process conformance' available under DOI: 10.1007/s00607-015-0441-1."
				+ "For a more user-friendly variant use the plug-in 'Multi-perspective Process Explorer' from the package: DataAwareExplorer.")
public final class BalancedDataConformancePlusPlugin
		extends AbstractBalancedDataConformancePlugin<BalancedReplayResult, BalancedDataAlignmentState> {

	/**
	 * Use on of the methods accepting {@link Petrinet} or {@link DataPetriNet}
	 * instead.
	 */
	@Deprecated
	public BalancedReplayResult balancedAlignmentPluginHeadless(final PluginContext context, PetrinetGraph net,
			XLog log, BalancedProcessorConfiguration config)
			throws ControlFlowAlignmentException, DataAlignmentException {
		return doBalancedAlignmentDataConformanceChecking(convertToDPN(net, context), log, context.getProgress(), config);
	}

	@UITopiaVariant(affiliation = UITopiaVariant.EHV, author = "F. Mannhardt, M. de Leoni", email = "f.mannhardt@tue.nl, m.d.leoni@tue.nl", pack = "DataAwareReplayer")
	@PluginVariant(variantLabel = "Conformance Checking of DPN (Balanced)", requiredParameterLabels = { 0, 1, 2 })
	public BalancedReplayResult balancedAlignmentPluginHeadless(final PluginContext context, Petrinet net, XLog log,
			BalancedProcessorConfiguration config) throws ControlFlowAlignmentException, DataAlignmentException {
		return doBalancedAlignmentDataConformanceChecking(convertToDPN(net, context), log, context.getProgress(), config);
	}

	@UITopiaVariant(affiliation = UITopiaVariant.EHV, author = "F. Mannhardt, M. de Leoni", email = "f.mannhardt@tue.nl, m.d.leoni@tue.nl", pack = "DataAwareReplayer")
	@PluginVariant(variantLabel = "Conformance Checking of DPN (Balanced)", requiredParameterLabels = { 0, 1, 2 })
	public BalancedReplayResult balancedAlignmentPluginHeadless(final PluginContext context, DataPetriNet dpn, XLog log,
			BalancedProcessorConfiguration config) throws ControlFlowAlignmentException, DataAlignmentException {
		return doBalancedAlignmentDataConformanceChecking(dpn, log, context.getProgress(), config);
	}

	/**
	 * Use on of the methods accepting {@link Petrinet} or {@link DataPetriNet}
	 * instead.
	 */
	@Deprecated
	public BalancedReplayResult balancedAlignmentPlugin(final UIPluginContext context, PetrinetGraph net, XLog log) {
		DataPetriNet dpnNet = convertToDPN(net, context);
		return balancedAlignmentPlugin(context, dpnNet, log);
	}

	@UITopiaVariant(affiliation = UITopiaVariant.EHV, author = "F. Mannhardt, M. de Leoni", email = "f.mannhardt@tue.nl, m.d.leoni@tue.nl", pack = "DataAwareReplayer")
	@PluginVariant(variantLabel = "Conformance Checking of DPN (Balanced)", requiredParameterLabels = { 0, 1 })
	public BalancedReplayResult balancedAlignmentPlugin(final UIPluginContext context, Petrinet net, XLog log) {
		DataPetriNet dpnNet = convertToDPN(net, context);
		return balancedAlignmentPlugin(context, dpnNet, log);
	}

	@UITopiaVariant(affiliation = UITopiaVariant.EHV, author = "F. Mannhardt, M. de Leoni", email = "f.mannhardt@tue.nl, m.d.leoni@tue.nl", pack = "DataAwareReplayer")
	@PluginVariant(variantLabel = "Conformance Checking of DPN (Balanced)", requiredParameterLabels = { 0, 1 })
	public BalancedReplayResult balancedAlignmentPlugin(final UIPluginContext context, DataPetriNet dpnNet, XLog log) {

		try {
			BalancedProcessorConfiguration config = AbstractBalancedDataConformancePlugin.queryConfiguration(context,
					dpnNet, log);
			try {
				BalancedReplayResult result = doBalancedAlignmentDataConformanceChecking(dpnNet, log,
						context.getProgress(), config);
				if (result.labelStepArray.size() != log.size()) {
					ProMUIHelper.showErrorMessage(context,
							String.format(
									"The alignment to the DPN could be computed for %s out of %s traces. Please check the activity log for more information why the alignment failed.",
									result.labelStepArray.size(), log.size()),
							"Alignment Failed (Partially)");
					context.getFutureResult(0).setLabel("Partial Result " + buildResultLabel(log, dpnNet));
					ProvidedObjectHelper.publish(context, "Partial Result " + buildResultLabel(log, dpnNet), result,
							BalancedReplayResult.class, false);
					ProvidedObjectHelper.setFavorite(context, result);
				} else {
					context.getFutureResult(0).setLabel(buildResultLabel(log, dpnNet));
				}
				return result;
			} catch (ControlFlowAlignmentException | DataAlignmentException e) {
				context.log(e);
				ProMUIHelper.showErrorMessage(context.getGlobalContext().getUI(), e.getMessage(), "Alignment failed",
						e);
				context.getFutureResult(0).cancel(true);
				return null;
			}
		} catch (UserCancelledException e) {
			context.getFutureResult(0).cancel(true);
			return null;
		}
	}

	/**
	 * Use
	 * {@link AbstractBalancedDataConformancePlugin#queryConfiguration(UIPluginContext, DataPetriNet, XLog)}
	 * instead!
	 */
	@Deprecated
	public static BalancedProcessorConfiguration queryConfiguration(final UIPluginContext context, DataPetriNet net,
			XLog log) throws UserCancelledException {
		return AbstractBalancedDataConformancePlugin.queryConfiguration(context, net, log);
	}

	/**
	 * Computes the {@link BalancedReplayResult} for the given
	 * {@link DataPetriNet} and the {@link XLog}.
	 * 
	 * @param net
	 *            a {@link PetrinetGraph} interpreted with DPN semantics
	 * @param log
	 * @param progressListener
	 * @param config
	 * @return the alignment result as {@link BalancedReplayResult}
	 * @throws DataAlignmentException
	 * @throws ControlFlowAlignmentException
	 */
	public BalancedReplayResult doBalancedAlignmentDataConformanceChecking(PetrinetGraph net, Collection<XTrace> log,
			Progress progressListener, BalancedProcessorConfiguration config)
			throws ControlFlowAlignmentException, DataAlignmentException {
		return doBalancedDataConformanceChecking(convertToDPN(net), log, progressListener, config);
	}

	@Override
	protected BalancedReplayResult preparePluginResult(DataPetriNet net, XLog log,
			DataConformancePlusConfiguration config, List<BalancedDataAlignmentState> dataAlignmentResults,
			long usedTime, AtomicDouble sumFitness) {

		config.getObserver().log(Level.INFO, "Preparing result ...");

		BalancedReplayResult resultReplay = new BalancedReplayResult(dataAlignmentResults, config.getVariableCost(),
				config.getVariableMapping(), net, log, config.getActivityMapping().getEventClassifier());

		if (config.getObserver() instanceof DataConformancePlusObserverImpl) {
			DataConformancePlusObserverImpl statisticsObserver = (DataConformancePlusObserverImpl) config.getObserver();
			Map<String, StatisticResult> statisticResults = statisticsObserver.getStatisticResults();
			resultReplay.getStatisticsStore().putAll(statisticResults);
		}

		resultReplay.setCalcTime(usedTime);

		return resultReplay;
	}

	@Override
	protected BalancedDataAlignmentState convertAlignmentResult(BalancedDataAlignmentState state,
			AtomicDouble sumFitness) {
		return state;
	}

}
