package org.processmining.plugins.balancedconformance;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.Timer;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.datapetrinets.DataPetriNet;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.PluginContextID;
import org.processmining.framework.plugin.Progress;
import org.processmining.framework.plugin.events.Logger;
import org.processmining.framework.plugin.events.ProgressEventListener;
import org.processmining.plugins.DataConformance.Alignment;
import org.processmining.plugins.DataConformance.visualization.alignment.XTraceResolver;
import org.processmining.plugins.DataConformance.visualization.grouping.GroupedAlignmentMasterDetail;
import org.processmining.plugins.DataConformance.visualization.grouping.GroupedAlignmentMasterView.GroupedAlignmentInput;
import org.processmining.plugins.DataConformance.visualization.grouping.GroupedAlignments;
import org.processmining.plugins.DataConformance.visualization.grouping.GroupedAlignmentsSimpleImpl;
import org.processmining.plugins.balancedconformance.config.BalancedProcessorConfiguration;
import org.processmining.plugins.balancedconformance.controlflow.ControlFlowAlignmentException;
import org.processmining.plugins.balancedconformance.dataflow.exception.DataAlignmentException;
import org.processmining.plugins.balancedconformance.export.XAlignmentConverter;
import org.processmining.plugins.balancedconformance.observer.DataConformancePlusObserver;
import org.processmining.plugins.balancedconformance.observer.DataConformancePlusObserverImpl;
import org.processmining.plugins.balancedconformance.observer.WrappedDataConformanceObserver;
import org.processmining.plugins.balancedconformance.result.BalancedDataAlignmentState;
import org.processmining.plugins.balancedconformance.result.BalancedReplayResult;
import org.processmining.plugins.connectionfactories.logpetrinet.TransEvClassMapping;
import org.processmining.plugins.utils.ProvidedObjectHelper;
import org.processmining.xesalignmentextension.XAlignmentExtension.XAlignment;

import com.fluxicon.slickerbox.factory.SlickerFactory;

final class LiveBalancedDataConformancePanel {

	private class WrappedProgress implements Progress {

		private final Progress progress;

		private WrappedProgress(Progress progress) {
			super();
			this.progress = progress;
		}

		public void setMinimum(int value) {
			progress.setMinimum(value);
		}

		public void setMaximum(int value) {
			progress.setMaximum(value);
		}

		public void setValue(int value) {
			progress.setValue(value);
		}

		public void setCaption(String message) {
			progress.setCaption(message);
		}

		public String getCaption() {
			return progress.getCaption();
		}

		public int getValue() {
			return progress.getValue();
		}

		public void inc() {
			progress.inc();
		}

		public void setIndeterminate(boolean makeIndeterminate) {
			progress.setIndeterminate(makeIndeterminate);
		}

		public boolean isIndeterminate() {
			return progress.isIndeterminate();
		}

		public int getMinimum() {
			return progress.getMinimum();
		}

		public int getMaximum() {
			return progress.getMaximum();
		}

		public boolean isCancelled() {
			return isCancelled;
		}

		public void cancel() {
			progress.cancel();
		}

	}

	public class WrappingLiveObserver extends WrappedDataConformanceObserver {

		private final XAlignmentConverter converter;

		public WrappingLiveObserver(DataConformancePlusObserver observer, TransEvClassMapping transEvClassMapping,
				Map<String, String> variableMapping) {
			super(observer);
			this.converter = new XAlignmentConverter().setClassifier(transEvClassMapping.getEventClassifier())
					.setVariableMapping(variableMapping);
		}

		public void calculatedFitness(int resultIndex, XTrace trace, BalancedDataAlignmentState resultState) {
			super.calculatedFitness(resultIndex, trace, resultState);
			Alignment alignment = BalancedReplayResult.createSmallAlignment(resultState,
					resultState.getOverallFitness(), null);
			groupedResult.add(converter.viewAsXAlignment(alignment, trace));
		}

	}

	private final BalancedProcessorConfiguration config;
	private final DataPetriNet net;
	private final XLog log;
	private final GroupedAlignments<XAlignment> groupedResult;

	private final JPanel mainPanel;
	private GroupedAlignmentMasterDetail visualization;

	private final JPanel bottomPanel;
	private final JProgressBar progressBar;
	private final JButton cancelButton;
	private final JTextArea logArea;

	protected boolean isCancelled = false;
	protected boolean isRunning = false;
	private WrappingLiveObserver liveObserver;
	private ProgressEventListener progressListener;
	private Logger logger;
	private Timer timer;
	protected int resultCount;

	public LiveBalancedDataConformancePanel(final BalancedProcessorConfiguration config, final DataPetriNet net,
			final XLog log) {
		this.config = config;
		this.net = net;
		this.log = log;
		this.groupedResult = new GroupedAlignmentsSimpleImpl(new HashMap<String, Color>());

		this.mainPanel = new JPanel();
		this.mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

		this.cancelButton = SlickerFactory.instance().createButton("Cancel");
		this.cancelButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				isCancelled = true;
				progressBar.setString("Cancelling ...");
				progressBar.setStringPainted(true);
			}
		});

		this.progressBar = SlickerFactory.instance().createProgressBar(JProgressBar.HORIZONTAL);

		this.bottomPanel = new JPanel();
		bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.Y_AXIS));
		bottomPanel.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.BLACK));
		JPanel progressPanel = new JPanel();
		progressPanel.setLayout(new BoxLayout(progressPanel, BoxLayout.X_AXIS));
		progressPanel.add(progressBar);
		progressPanel.add(cancelButton);

		bottomPanel.add(progressPanel);
		logArea = new JTextArea(300, 1);
		logArea.setWrapStyleWord(true);
		logArea.setLineWrap(true);
		bottomPanel.add(new JScrollPane(logArea));

		logger = new Logger() {

			public void log(final String message, PluginContextID contextID, MessageLevel messageLevel) {
				EventQueue.invokeLater(new Runnable() {

					public void run() {
						logArea.append(message.concat("\n"));
					}
				});
			}

			public void log(final Throwable t, PluginContextID contextID) {
				EventQueue.invokeLater(new Runnable() {

					public void run() {
						logArea.append(t.toString().concat("\n"));
					}
				});
			}
		};
		progressListener = new ProgressEventListener() {

			public void changeProgressIndeterminate(final boolean indeterminate) {
				EventQueue.invokeLater(new Runnable() {

					public void run() {
						progressBar.setIndeterminate(indeterminate);
					}
				});

			}

			public void changeProgressCaption(final String newCaption) {
				EventQueue.invokeLater(new Runnable() {

					public void run() {
						progressBar.setString(newCaption);
					}
				});
			}

			public void changeProgressBounds(final int lowBo, final int upBo) {
				EventQueue.invokeLater(new Runnable() {

					public void run() {
						progressBar.setMinimum(lowBo);
						progressBar.setMaximum(upBo);
					}
				});
			}

			public void changeProgress(final int progress) {
				EventQueue.invokeLater(new Runnable() {

					public void run() {
						progressBar.setValue(progress);
					}
				});
			}
		};
	}

	private static Map<String, XTrace> buildTraceMap(XLog log) {
		HashMap<String, XTrace> traceMap = new HashMap<>();
		for (XTrace trace : log) {
			traceMap.put(XConceptExtension.instance().extractName(trace), trace);
		}
		return traceMap;
	}

	public JPanel show(final PluginContext context) {
		if (isRunning || isCancelled) {
			return mainPanel;
		}

		// Add listener to context
		context.getProgressEventListeners().add(progressListener);
		context.getLoggingListeners().add(logger);

		// Start Visualization in current context
		final Map<String, XTrace> traceMap = buildTraceMap(log);
		visualization = new GroupedAlignmentMasterDetail(context, new GroupedAlignmentInput<>(groupedResult,
				new XTraceResolver() {

					public boolean hasOriginalTraces() {
						return true;
					}

					public XTrace getOriginalTrace(String name) {
						return traceMap.get(name);
					}
				}, Collections.<String, Color>emptyMap()));		
		visualization.setMaximumSize(visualization.getMaximumSize());
		visualization.disableSearch();
		visualization.disableColorCodeActivities();

		// Add all components to our main panel
		mainPanel.add(visualization);
		mainPanel.add(bottomPanel);

		// Wire up the observers
		liveObserver = new WrappingLiveObserver(new DataConformancePlusObserverImpl(context),
				config.getActivityMapping(), config.getVariableMapping());
		config.setObserver(liveObserver);

		timer = new Timer(1000, new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				if (groupedResult.size() > resultCount) {
					visualiseResult();
					resultCount = groupedResult.size();
				}
			}
		});

		final ExecutorService executor = Executors.newSingleThreadExecutor();
		Runnable runnable = new Runnable() {

			public void run() {
				isRunning = true;
				BalancedDataConformancePlusPlugin conformancePlusPlugin = new BalancedDataConformancePlusPlugin();
				try {
					BalancedReplayResult replayResult = conformancePlusPlugin
							.doBalancedAlignmentDataConformanceChecking(net, log,
									new WrappedProgress(context.getProgress()), config);
					if (liveObserver.getWrappedObserver() instanceof DataConformancePlusObserverImpl) {
						DataConformancePlusObserverImpl statisticsObserver = (DataConformancePlusObserverImpl) liveObserver
								.getWrappedObserver();
						replayResult.getStatisticsStore().putAll(statisticsObserver.getStatisticResults());
					}
					timer.stop();
					isRunning = false;
					cancelButton.setVisible(false);
					progressBar.setVisible(false);
					EventQueue.invokeLater(new Runnable() {

						public void run() {
							visualization.enableSearch();
							visualiseResult();
						}

					});

					int computedAlignments = replayResult.labelStepArray.size();
					int requiredAlignments = log.size();
					if (computedAlignments != requiredAlignments) {
						Object[] options = { "OK" };

						JOptionPane
								.showOptionDialog(
										null,
										String.format(
												"The alignment to the DPN could be computed for %s out of %s traces. Please check the log window for more information.",
												computedAlignments, requiredAlignments),
										"Alignment Failed (Partially)", JOptionPane.PLAIN_MESSAGE,
										JOptionPane.ERROR_MESSAGE, null, options, options[0]);
					}

					context.getProvidedObjectManager().createProvidedObject(
							BalancedDataConformancePlusPlugin.buildResultLabel(log, net), replayResult,
							BalancedReplayResult.class, context);

					ProvidedObjectHelper.setFavorite(context, replayResult);

				} catch (ControlFlowAlignmentException e) {
					context.log(e);
				} catch (DataAlignmentException e) {
					context.log(e);
				}
			}

		};
		executor.execute(runnable);
		timer.start();
		return mainPanel;
	}

	private void visualiseResult() {
		visualization.replaceResult(groupedResult);
	}

}
