package org.processmining.plugins.balancedconformance;

import java.util.concurrent.Callable;

import org.deckfour.xes.model.XTrace;
import org.processmining.plugins.balancedconformance.observer.DataConformancePlusObserver;
import org.processmining.plugins.balancedconformance.result.BalancedDataAlignmentState;
import org.processmining.plugins.balancedconformance.result.DataAlignedTrace;
import org.processmining.plugins.balancedconformance.result.MaxAlignmentCostHelper;

public final class BalancedReplayResultProcessor implements Callable<BalancedDataAlignmentState> {

	private final DataAlignmentTraceProcessor dataAlignmentTraceProcessor;
	private final MaxAlignmentCostHelper maxAlignmentCostHelper;
	private final DataConformancePlusObserver observer;

	private final XTrace trace;
	private final int traceIndex;
	private final int groupSize;

	public BalancedReplayResultProcessor(int traceIndex, XTrace trace, MaxAlignmentCostHelper maxAlignmentCostHelper,
			DataAlignmentTraceProcessor dataAlignmentTraceProcessor, DataConformancePlusObserver observer) {
		this(traceIndex, trace, maxAlignmentCostHelper, dataAlignmentTraceProcessor, observer, 1);
	}

	public BalancedReplayResultProcessor(int traceIndex, XTrace trace, MaxAlignmentCostHelper maxAlignmentCostHelper,
			DataAlignmentTraceProcessor dataAlignmentTraceProcessor, DataConformancePlusObserver observer,
			int groupSize) {
		this.traceIndex = traceIndex;
		this.trace = trace;
		this.maxAlignmentCostHelper = maxAlignmentCostHelper;
		this.dataAlignmentTraceProcessor = dataAlignmentTraceProcessor;
		this.observer = observer;
		this.groupSize = groupSize;
	}

	public BalancedDataAlignmentState call() throws Exception {
		DataAlignedTrace dataAlignment = dataAlignmentTraceProcessor.computeDataAlignmentForTrace();

		// just supporting integer cost
		int maxCost = (int) maxAlignmentCostHelper.getMaxCost(trace);

		BalancedDataAlignmentState resultState = new BalancedDataAlignmentState(dataAlignment.getLogTrace(),
				dataAlignment.getProcessTrace(), dataAlignment.getOverallCost(), dataAlignment.getDataCost(), maxCost,
				trace);

		observer.calculatedFitness(traceIndex, trace, resultState);

		return resultState;
	}

	public int getGroupSize() {
		return groupSize;
	}

	public int getTraceIndex() {
		return traceIndex;
	}

	public XTrace getTrace() {
		return trace;
	}

}
