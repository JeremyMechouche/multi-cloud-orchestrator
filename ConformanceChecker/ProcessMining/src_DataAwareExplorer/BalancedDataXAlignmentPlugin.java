package org.processmining.plugins.balancedconformance;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.datapetrinets.DataPetriNet;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.Progress;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginCategory;
import org.processmining.framework.plugin.annotations.PluginLevel;
import org.processmining.framework.plugin.annotations.PluginVariant;
import org.processmining.framework.util.ui.widgets.helper.ProMUIHelper;
import org.processmining.framework.util.ui.widgets.helper.UserCancelledException;
import org.processmining.models.graphbased.directed.petrinet.Petrinet;
import org.processmining.models.graphbased.directed.petrinet.PetrinetGraph;
import org.processmining.plugins.DataConformance.Alignment;
import org.processmining.plugins.DataConformance.framework.ExecutionStep;
import org.processmining.plugins.DataConformance.visualization.DataAwareStepTypes;
import org.processmining.plugins.balancedconformance.config.BalancedProcessorConfiguration;
import org.processmining.plugins.balancedconformance.config.DataConformancePlusConfiguration;
import org.processmining.plugins.balancedconformance.controlflow.ControlFlowAlignmentException;
import org.processmining.plugins.balancedconformance.dataflow.exception.DataAlignmentException;
import org.processmining.plugins.balancedconformance.export.XAlignmentConverter;
import org.processmining.plugins.balancedconformance.result.BalancedDataAlignmentState;
import org.processmining.plugins.utils.ProvidedObjectHelper;
import org.processmining.xesalignmentextension.XAlignmentExtension;
import org.processmining.xesalignmentextension.XDataAlignmentExtension;
import org.xeslite.external.XFactoryExternalStore;

import com.google.common.util.concurrent.AtomicDouble;

/**
 * Same as {@link BalancedDataConformancePlusPlugin} but returns an aligned
 * {@link XLog} with information about the alignment encoded using the
 * {@link XAlignmentExtension}.
 * 
 * @author F. Mannhardt
 *
 */
@Plugin(name = "Conformance Checking of DPN (XLog)", level = PluginLevel.Regular, parameterLabels = { "Data Petri net",
		"Log", "Configuration" }, returnLabels = { "XLog with Alignment (XAlignmentExtension)" }, returnTypes = {
				XLog.class }, userAccessible = true, handlesCancel = true, categories = PluginCategory.ConformanceChecking, keywords = {
						"conformance", "DPN", "data", "alignment" }, //
		help = "Conformance Checking of a Data Petri Net (DPN) with regard to an event log."
				+ "This plug-in returns an aligned version of the input event log containing the alignment information using the XES alignment extension."
				+ "The technique is described in the article 'Balanced multi-perspective checking of process conformance' available under DOI: 10.1007/s00607-015-0441-1."
				+ "For a more user-friendly variant use the plug-in 'Multi-perspective Process Explorer' from the package: DataAwareExplorer.")
public final class BalancedDataXAlignmentPlugin extends AbstractBalancedDataConformancePlugin<XLog, XTrace> {

	private XAlignmentConverter converter = new XAlignmentConverter()
			.setFactory(new XFactoryExternalStore.InMemoryStoreAlignmentAwareImpl());

	/**
	 * Use
	 * {@link #alignLog(Progress, PetrinetGraph, XLog, BalancedProcessorConfiguration)}
	 * instead
	 */
	@Deprecated
	public XLog alignLogPluginConvertFromPetrinet(final PluginContext context, PetrinetGraph net, XLog log,
			BalancedProcessorConfiguration config) throws ControlFlowAlignmentException, DataAlignmentException {
		return alignLogPluginConvertFromPetrinet(context, convertToDPN(net, context), log, config);
	}

	@UITopiaVariant(affiliation = UITopiaVariant.EHV, author = "F. Mannhardt", email = "f.mannhardt@tue.nl", pack = "DataAwareReplayer")
	@PluginVariant(variantLabel = "Conformance Checking of DPN (XLog with XAlignmentExtension)", requiredParameterLabels = {
			0, 1, 2 })
	public XLog alignLogPluginConvertFromPetrinet(final PluginContext context, Petrinet net, XLog log,
			BalancedProcessorConfiguration config) throws ControlFlowAlignmentException, DataAlignmentException {
		return alignLogPluginConvertFromPetrinet(context, convertToDPN(net, context), log, config);
	}

	@UITopiaVariant(affiliation = UITopiaVariant.EHV, author = "F. Mannhardt", email = "f.mannhardt@tue.nl", pack = "DataAwareReplayer")
	@PluginVariant(variantLabel = "Conformance Checking of DPN (XLog with XAlignmentExtension)", requiredParameterLabels = {
			0, 1, 2 })
	public XLog alignLogPluginConvertFromPetrinet(final PluginContext context, DataPetriNet dpn, XLog log,
			BalancedProcessorConfiguration config) throws ControlFlowAlignmentException, DataAlignmentException {
		context.getFutureResult(0).setLabel(buildResultLabel(log, dpn));
		XLog alignedLog = alignLog(context.getProgress(), dpn, log, config);
		return alignedLog;
	}

	/**
	 * Use
	 * {@link #alignLog(Progress, PetrinetGraph, XLog, BalancedProcessorConfiguration)}
	 * instead
	 */
	@Deprecated
	public XLog alignLogPlugin(final UIPluginContext context, PetrinetGraph net, XLog log) {
		return alignLogPlugin(context, convertToDPN(net, context), log);
	}

	@UITopiaVariant(affiliation = UITopiaVariant.EHV, author = "F. Mannhardt", email = "f.mannhardt@tue.nl", pack = "DataAwareReplayer")
	@PluginVariant(variantLabel = "Conformance Checking of DPN (XLog with XAlignmentExtension)", requiredParameterLabels = {
			0, 1 })
	public XLog alignLogPlugin(final UIPluginContext context, Petrinet net, XLog log) {
		return alignLogPlugin(context, convertToDPN(net, context), log);
	}

	@UITopiaVariant(affiliation = UITopiaVariant.EHV, author = "F. Mannhardt", email = "f.mannhardt@tue.nl", pack = "DataAwareReplayer")
	@PluginVariant(variantLabel = "Conformance Checking of DPN (XLog with XAlignmentExtension)", requiredParameterLabels = {
			0, 1 })
	public XLog alignLogPlugin(final UIPluginContext context, DataPetriNet dpn, XLog log) {
		try {
			BalancedProcessorConfiguration config = AbstractBalancedDataConformancePlugin.queryConfiguration(context,
					dpn, log);
			try {
				XLog result = alignLog(context.getProgress(), dpn, log, config);
				if (result.size() != log.size()) {
					ProMUIHelper.showErrorMessage(context,
							String.format(
									"The alignment to the DPN could be computed for %s out of %s traces. Please check the activity log for more information why the alignment failed.",
									result.size(), log.size()),
							"Alignment Failed (Partially)");
					context.getFutureResult(0).setLabel("Partial Result " + buildResultLabel(log, dpn));
					ProvidedObjectHelper.publish(context, "Partial Result " + buildResultLabel(log, dpn), result,
							XLog.class, true);
				} else {
					context.getFutureResult(0).setLabel(buildResultLabel(log, dpn));
				}
				return result;
			} catch (ControlFlowAlignmentException | DataAlignmentException e) {
				context.log(e);
				ProMUIHelper.showErrorMessage(context.getGlobalContext().getUI(), e.getMessage(), "Alignment failed",
						e);
				context.getFutureResult(0).cancel(true);
				return null;
			}
		} catch (UserCancelledException e) {
			context.getFutureResult(0).cancel(true);
			return null;
		}
	}

	public XLog alignLog(PetrinetGraph net, Collection<XTrace> log, BalancedProcessorConfiguration config)
			throws ControlFlowAlignmentException, DataAlignmentException {
		return alignLog(new ProgressNoOpImpl(), net, log, config);
	}

	public XLog alignLog(final Progress progress, PetrinetGraph net, Collection<XTrace> log,
			BalancedProcessorConfiguration config) throws ControlFlowAlignmentException, DataAlignmentException {
		DataPetriNet dpn = convertToDPN(net);
		converter.setClassifier(config.getActivityMapping().getEventClassifier());
		converter.setVariableMapping(config.getVariableMapping());
		return doBalancedDataConformanceChecking(dpn, log, progress, config);
	}

	@Override
	protected XLog preparePluginResult(DataPetriNet net, XLog log, DataConformancePlusConfiguration config,
			List<XTrace> dataAlignmentResults, long usedTime, AtomicDouble sumFitness) {

		XLog extendedLog = converter.getFactory().createLog((XAttributeMap) log.getAttributes().clone());
		extendedLog.getClassifiers().addAll(log.getClassifiers());
		extendedLog.getExtensions().addAll(log.getExtensions());
		extendedLog.getGlobalTraceAttributes().addAll(log.getGlobalTraceAttributes());
		extendedLog.getGlobalEventAttributes().addAll(log.getGlobalEventAttributes());

		XAlignmentExtension alignmentExtension = XAlignmentExtension.instance();
		XDataAlignmentExtension dataAlignmentExtension = XDataAlignmentExtension.instance();
		XConceptExtension conceptExtension = XConceptExtension.instance();

		extendedLog.getExtensions().add(conceptExtension);
		extendedLog.getExtensions().add(alignmentExtension);
		extendedLog.getExtensions().add(dataAlignmentExtension);

		int numTraces = 0;
		for (XTrace trace : dataAlignmentResults) {
			if (trace != null) {
				extendedLog.add(trace);
				numTraces++;
			}
		}
		alignmentExtension.assignFitness(extendedLog, sumFitness.doubleValue() / numTraces);
		sumFitness.set(0.0);

		if (converter.getFactory() instanceof XFactoryExternalStore) {
			((XFactoryExternalStore) converter.getFactory()).commit();
		}

		return extendedLog;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.processmining.plugins.balancedconformance.
	 * AbstractBalancedDataConformancePlugin
	 * #convertAlignmentResult(org.processmining
	 * .plugins.balancedconformance.result.BalancedDataAlignmentState)
	 */
	@Override
	protected XTrace convertAlignmentResult(BalancedDataAlignmentState state, AtomicDouble sumFitness) {

		XTrace newTrace = converter.getFactory()
				.createTrace((XAttributeMap) state.getOriginalTrace().getAttributes().clone());

		XAlignmentExtension.instance().assignFitness(newTrace, state.getOverallFitness());
		sumFitness.addAndGet(state.getOverallFitness());

		Iterator<ExecutionStep> logTraceIter = state.getLogTracePrefix().iterator();
		Iterator<ExecutionStep> processTraceIter = state.getProcessTracePrefix().iterator();

		while (logTraceIter.hasNext()) {
			assert processTraceIter.hasNext() : "Inconsistent result process trace and log trace differ in size";

			ExecutionStep processStep = processTraceIter.next();
			ExecutionStep logStep = logTraceIter.next();
			DataAwareStepTypes stepType;

			if (logStep == ExecutionStep.bottomStep || logStep.getActivity() == null) {
				if (processStep.isInvisible()) {
					stepType = DataAwareStepTypes.MINVI;
				} else {
					stepType = DataAwareStepTypes.MREAL;
				}
			} else if (processStep == ExecutionStep.bottomStep) {
				stepType = DataAwareStepTypes.L;
			} else {
				if (processStep.equals(logStep)) {
					stepType = DataAwareStepTypes.LMGOOD;
				} else {
					stepType = DataAwareStepTypes.LMNOGOOD;
				}
			}

			newTrace.add(converter.copyAsXEvent(new Alignment.AlignmentStepImpl(stepType, processStep, logStep)));
		}

		return newTrace;
	}

	public XAlignmentConverter getConverter() {
		return converter;
	}

	public void setConverter(XAlignmentConverter converter) {
		this.converter = converter;
	}

}
