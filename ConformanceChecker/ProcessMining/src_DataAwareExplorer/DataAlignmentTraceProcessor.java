/*
 * Copyright (c) 2014 F. Mannhardt (f.mannhardt@tue.nl)
 */
package org.processmining.plugins.balancedconformance;

import java.util.concurrent.Callable;

import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.datapetrinets.DataPetriNet;
import org.processmining.log.utils.XLogBuilder;
import org.processmining.log.utils.XUtils;
import org.processmining.plugins.balancedconformance.config.BalancedProcessorConfiguration;
import org.processmining.plugins.balancedconformance.controlflow.ControlFlowAlignmentException;
import org.processmining.plugins.balancedconformance.controlflow.ControlFlowAlignmentResult;
import org.processmining.plugins.balancedconformance.controlflow.UnreliableControlFlowAlignmentException;
import org.processmining.plugins.balancedconformance.controlflow.adapter.AlignmentAdapter;
import org.processmining.plugins.balancedconformance.controlflow.resultbuilder.AlignmentResultBuilder;
import org.processmining.plugins.balancedconformance.dataflow.DataAlignmentAdapter;
import org.processmining.plugins.balancedconformance.dataflow.exception.DataAlignmentException;
import org.processmining.plugins.balancedconformance.dataflow.exception.UnreliableDataAlignmentException;
import org.processmining.plugins.balancedconformance.dataflow.exception.UnreliableDataAlignmentException.UnreliableReason;
import org.processmining.plugins.balancedconformance.functions.VirtualVariable;
import org.processmining.plugins.balancedconformance.mapping.LogMapping;
import org.processmining.plugins.balancedconformance.mapping.TraceMapping;
import org.processmining.plugins.balancedconformance.observer.DataConformancePlusObserver;
import org.processmining.plugins.balancedconformance.observer.DataConformancePlusObserverNoOpImpl;
import org.processmining.plugins.balancedconformance.result.DataAlignedTrace;

/**
 * Provides the optimal alignment of a {@link XTrace} and a {@link DataPetriNet}
 * . Use {@link #computeDataAlignmentForTrace()} to get the alignment.
 * 
 * @author F. Mannhardt
 * 
 */
public final class DataAlignmentTraceProcessor implements Callable<DataAlignedTrace> {

	// Debug message when time limit in microseconds is exceeded
	private static final int DEFAULT_SLOW_TRACE_LIMIT = 50 * 1000 * 1000;

	private final int traceIndex;
	private final XTrace trace;
	private final LogMapping logMapping;

	private final DataConformancePlusObserver observer;
	private final AlignmentAdapter controlFlowAlignmentAdapter;
	private final DataAlignmentAdapter dataAlignmentAdapter;
	private final double costLimit;

	private boolean selfClosed = false;

	private final boolean usePartialOrder;

	/**
	 * Create a {@link DataAlignmentTraceProcessor} using the specified
	 * {@link BalancedProcessorConfiguration}.
	 * 
	 * @param trace
	 * @param net
	 * @param config
	 * @throws DataAlignmentException
	 * @throws ControlFlowAlignmentException
	 */
	public DataAlignmentTraceProcessor(XTrace trace, DataPetriNet net, BalancedProcessorConfiguration config)
			throws DataAlignmentException, ControlFlowAlignmentException {
		this(trace, net, config, new DataConformancePlusObserverNoOpImpl(), Integer.MAX_VALUE);
	}

	/**
	 * Create a {@link DataAlignmentTraceProcessor} using the specified
	 * {@link BalancedProcessorConfiguration}.
	 * 
	 * @param trace
	 * @param net
	 * @param config
	 * @param observer
	 *            for information about the computation
	 * @throws DataAlignmentException
	 * @throws ControlFlowAlignmentException
	 */
	public DataAlignmentTraceProcessor(XTrace trace, DataPetriNet net, BalancedProcessorConfiguration config,
			DataConformancePlusObserver observer) throws DataAlignmentException, ControlFlowAlignmentException {
		this(trace, net, config, observer, Integer.MAX_VALUE);
	}

	/**
	 * Create a {@link DataAlignmentTraceProcessor} using the specified
	 * {@link BalancedProcessorConfiguration}. Also specifying a
	 * 'slowTraceLimit'.
	 * 
	 * @param trace
	 * @param net
	 * @param config
	 * @param observer
	 *            for information about the computation
	 * @param slowTraceLimit
	 *            in milliseconds
	 * @throws DataAlignmentException
	 * @throws ControlFlowAlignmentException
	 */
	public DataAlignmentTraceProcessor(XTrace trace, DataPetriNet net, BalancedProcessorConfiguration config,
			DataConformancePlusObserver observer, int costLimit) throws DataAlignmentException,
			ControlFlowAlignmentException {
		this(trace, net, config, observer, costLimit, false);
	}

	public DataAlignmentTraceProcessor(XTrace trace, DataPetriNet net, BalancedProcessorConfiguration config,
			DataConformancePlusObserver observer, int costLimit, boolean usePartialorder)
			throws DataAlignmentException, ControlFlowAlignmentException {
		usePartialOrder = usePartialorder;
		this.traceIndex = 0;
		this.trace = trace;

		XLog wrapperLog = XLogBuilder.newInstance().startLog("WRAPPER").build();
		wrapperLog.add(trace);

		this.logMapping = new LogMapping(config.getActivityMapping(), config.getMapEvClass2Cost(),
				config.getMapTrans2Cost(), config.getVariableMapping(), config.getVariableCost(),
				config.getUpperBounds(), config.getLowerBounds(), config.getDefaultValues(),
				config.getVariablesUnassignedMode(), net, wrapperLog);

		this.dataAlignmentAdapter = AbstractBalancedDataConformancePlugin.createDataAlignmentAdapter(config,
				logMapping, net);
		this.controlFlowAlignmentAdapter = AbstractBalancedDataConformancePlugin.createControlFlowAlignmentAdapter(net,
				wrapperLog, config, logMapping, dataAlignmentAdapter, observer);

		this.selfClosed = true;

		this.costLimit = costLimit;
		this.observer = observer;
	}

	/**
	 * Create a {@link DataAlignmentTraceProcessor} using specified
	 * {@link AlignmentAdapter} and {@link DataAlignmentAdapter}. Only
	 * recommended for expert usage, in doubt use
	 * {@link #DataAlignmentTraceProcessor(XTrace, DataPetriNet, BalancedProcessorConfiguration, DataConformancePlusObserver)}
	 * instead.
	 * 
	 * @param traceIndex
	 * @param trace
	 * @param logMapping
	 * @param controlFlowAlignmentAdapter
	 * @param dataAlignmentAdapter
	 * @param costLimit
	 * @param observer
	 */
	public DataAlignmentTraceProcessor(int traceIndex, XTrace trace, LogMapping logMapping,
			AlignmentAdapter controlFlowAlignmentAdapter, DataAlignmentAdapter dataAlignmentAdapter, double costLimit,
			DataConformancePlusObserver observer, boolean usePartialorder) {
		this.traceIndex = traceIndex;
		this.trace = trace;
		this.logMapping = logMapping;
		this.controlFlowAlignmentAdapter = controlFlowAlignmentAdapter;
		this.dataAlignmentAdapter = dataAlignmentAdapter;
		this.costLimit = costLimit;
		this.observer = observer;
		usePartialOrder = usePartialorder;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public DataAlignedTrace call() throws Exception {
		return computeDataAlignmentForTrace();
	}

	/**
	 * @return a {@link DataAlignedTrace}
	 * @throws ControlFlowAlignmentException
	 *             in case something goes wrong in the control flow part of the
	 *             search
	 * @throws DataAlignmentException
	 *             in case something goes wrong in the data part of the search
	 */
	public DataAlignedTrace computeDataAlignmentForTrace() throws ControlFlowAlignmentException, DataAlignmentException {

		observer.processingNewTrace(traceIndex, trace);
		long startTime = System.nanoTime();

		TraceMapping traceMapping = new TraceMapping(trace, logMapping);

		AlignmentResultBuilder resultBuilder = controlFlowAlignmentAdapter.calcReplayResult(traceMapping,
				usePartialOrder);

		ControlFlowAlignmentResult controlFlowResult = null;
		try {
			controlFlowResult = resultBuilder.calcResult(costLimit);
		} catch (UnreliableControlFlowAlignmentException e) {
			switch (e.getReason()) {
				case CANCELLED :
					observer.unreliableAlignmentDetected(traceIndex, trace);
					throw new UnreliableDataAlignmentException(e, UnreliableReason.CANCELLED, trace);

				case MAXCOST :
					observer.unreliableAlignmentDetected(traceIndex, trace);
					throw new UnreliableDataAlignmentException(String.format(
							"Unreliable Data Alignment for trace %s, message %s", XUtils.getConceptName(trace),
							e.toString()), e, UnreliableReason.MAXCOST, trace);
				case IMPOSSIBLE :
					observer.unreliableAlignmentDetected(traceIndex, trace);
					throw new UnreliableDataAlignmentException("Unreliable Data Alignment " + e.getMessage(), e,
							UnreliableReason.IMPOSSIBLE, trace);
				default :
					throw new RuntimeException(e);
			}

		}

		DataAlignedTrace dataAlignment = dataAlignmentAdapter
				.retrieveFullDataAlignment(traceMapping, controlFlowResult);

		long endTime = System.nanoTime();
		long usedTime = (endTime - startTime) / 1000;

		if (usedTime > DEFAULT_SLOW_TRACE_LIMIT) {
			observer.slowDataAlignmentDetected(traceIndex, trace, dataAlignment, usedTime);
		}

		//TODO somehow change this
		// Remove virtual variable
		if (logMapping.hasVirtualVariables()) {
			for (XEvent event : trace) {
				event.getAttributes().remove(VirtualVariable.ATTRIBUTE_KEY_RELATIVE_TIME);
			}
		}

		observer.foundOptimalAlignment(traceIndex, trace, dataAlignment, resultBuilder.getPartialDataAlignments(),
				resultBuilder.getCacheHit(), resultBuilder.getCacheSize(), resultBuilder.getTotalQueuedStates(),
				resultBuilder.getDataStateCount(), usedTime);

		if (selfClosed) {
			dataAlignmentAdapter.close();
			controlFlowAlignmentAdapter.close();
		}

		return dataAlignment;
	}

	public int getTraceIndex() {
		return traceIndex;
	}

	public XTrace getTrace() {
		return trace;
	}

}