package org.processmining.plugins.balancedconformance;

import org.deckfour.xes.model.XLog;
import org.processmining.contexts.uitopia.UIPluginContext;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.datapetrinets.DataPetriNet;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginCategory;
import org.processmining.framework.plugin.annotations.PluginLevel;
import org.processmining.framework.util.ui.widgets.helper.UserCancelledException;
import org.processmining.plugins.balancedconformance.config.BalancedProcessorConfiguration;

/**
 * Shows the alignment results 'live' as they are computed.
 * 
 * @author F. Mannhardt
 *
 */
public final class LiveBalancedDataConformancePlugin {

	@Plugin(name = "Conformance Checking of DPN (Live)", level = PluginLevel.NightlyBuild, parameterLabels = { "Petri Net With Data", "Log" }, returnLabels = { "Data Conformance Progress" }, returnTypes = { LiveBalancedDataConformancePanel.class }, userAccessible = true, handlesCancel = true, categories = PluginCategory.ConformanceChecking, keywords = {
			"conformance", "DPN", "data", "alignment" }, //
	help = "Conformance Checking of a Petri net with Data with regard to an Event Log. "
			+ "The technique is described in the article 'Balanced multi-perspective checking of process conformance' available under DOI: 10.1007/s00607-015-0441-1.")
	@UITopiaVariant(affiliation = UITopiaVariant.EHV, author = "F. Mannhardt", email = "f.mannhardt@tue.nl", pack = "DataAwareReplayer")
	public LiveBalancedDataConformancePanel singleAlignmentPluginWithProgress(final UIPluginContext context,
			DataPetriNet net, XLog log) {
		try {
			BalancedProcessorConfiguration config = AbstractBalancedDataConformancePlugin.queryConfiguration(context,
					net, log);
			context.getFutureResult(0).setLabel(
					"Live View of " + BalancedDataConformancePlusPlugin.buildResultLabel(log, net));
			return new LiveBalancedDataConformancePanel(config, net, log);
		} catch (UserCancelledException e) {
			context.getFutureResult(0).cancel(true);
			return null;
		}
	}

}
