package org.processmining.plugins.balancedconformance;

import javax.swing.JComponent;

import org.processmining.contexts.uitopia.annotations.Visualizer;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;
import org.processmining.framework.plugin.annotations.PluginVariant;

public class LiveBalancedDataConformanceVisualizer {

	@Plugin(name = "Conformance Checking of DPN (Live View)", returnLabels = { "Progress" }, returnTypes = { JComponent.class }, userAccessible = true, parameterLabels = { "Matching Instances" })
	@PluginVariant(requiredParameterLabels = { 0 })
	@Visualizer
	public JComponent visualise(PluginContext context, LiveBalancedDataConformancePanel progress) {
		return progress.show(context);
	}

}