package org.processmining.plugins.balancedconformance;

import org.processmining.framework.plugin.Progress;

final class ProgressNoOpImpl implements Progress {

	public void setValue(int value) {
	}

	public void setMinimum(int value) {
	}

	public void setMaximum(int value) {
	}

	public void setIndeterminate(boolean makeIndeterminate) {
	}

	public void setCaption(String message) {
	}

	public boolean isIndeterminate() {
		return false;
	}

	public boolean isCancelled() {
		return false;
	}

	public void inc() {
	}

	public int getValue() {
		return 0;
	}

	public int getMinimum() {
		return 0;
	}

	public int getMaximum() {
		return 0;
	}

	public String getCaption() {
		return "";
	}

	public void cancel() {
	}

}