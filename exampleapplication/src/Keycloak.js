import Keycloak from "keycloak-js";
const keycloak = new Keycloak({
 url: "http://192.168.56.124:8080/auth",
 realm: "ApplicationTest",
 clientId: "React-auth",
});

export default keycloak;